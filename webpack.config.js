const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    context     : path.resolve(__dirname, './src'),
    entry       : './index.ts',
    output      : {
        filename     : 'bundle.js',
        path         : path.resolve(__dirname, 'dist'),
        library      : 'Game',
        libraryTarget: 'umd',
    },
    devServer   : {
        contentBase: './dist',
        openPage   : 'main.html'
    },
    devtool     : "source-map",
    resolve     : {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache        : true,
                parallel     : true,
                uglifyOptions: {
                    compress       : true,
                    ecma           : 6,
                    keep_fnames    : true,
                    keep_classnames: true,
                },
                sourceMap    : true
            })

        ]
    },
    module      : {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            {test: /\.tsx?$/, loader: "awesome-typescript-loader"},

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {test: /\.js$/, loader: "source-map-loader"},
            {
                test   : /\.js$/,
                include: [/src/],
                use    : {
                    loader : 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    },
    plugins     : [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
            },
            __VERSION__  : JSON.stringify(require("./lib/dad/package.json").version),
            __RPG_VERSION__  : JSON.stringify(require("./package.json").version),
        })
    ]
};
