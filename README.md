## Getting started with development

1. `git submodule init`
2. `npm install`
3. `npm run start`

## Design document
### Architecture
- organized in a finite state machine (FSM)
- state/screens holds as much local data as possible
    - knows how to render itself
    - handles input
    - handles tick updates (only some states allow time passage)
- non-exhaustic list of states/screens
    - local movement (main play screen)
        - owner of map
        - reference to player
    - other screens have more limited transitions/functionality and don't own persistent data
        - context menu for actions at location and gateway to other screens
        - detailed combat
        - examination
        - inventory
- shared data
    - game object holds configuration values and HTML elements
        - responsible for creating and placement of HTML elements
        - API for accessing elements and rendering to them
- map
    - 2D tiles background/environment
    - continuous (non-tile) entities with radii
    - owns all entities
    - owns all tiles
- entity
    - physical thing in the map with some uid
    - most interactions are between entities
- actions
    - entities that are actors can take actions
    - queued up and executed simultaneously for all entities
    - entity objects never modifies other entities directly, and does so through actions
    - may or may not have an owner and may or may not have a target
    - NPCs queue actions from behaviors
    - PC queue actions from direct input (mouse + keyboard)
- behaviors
    - actors have a sequence of behaviors ordered by priority that make up their "AI"
    - each computes what actions to queue/modify

### Units
- canvas units at the entity API level
- canvas units and tile units (when explicit) for the map API
- speeds are in canvas units per 1 second

### Responsibilities
- States should hold all necessary data inside of it and not offload to the global Data object

### Entity-Entity relationships
- be able to model
    - violent disposition (prefer violence over sex)
    - lustful disposition (prefer sex over violence)
    - running away because of fear
    - slave gradually subverting a master's power
- opinion system
    - awe
    - hate
    - lust
- personality system works together with opinion
    - confidence (vs awe) = terror
    - composure (vs hate) = aggression
    - modesty (vs lust) = infatuation
- opinion split into base and modifiers
    - base is long term and remembered
    - modifier is based on circumstance
- dynamically changes based on interactions
    - clothing worn
    - past actions (e.g. aggression)
    - recent actions
        - modifier rather than modifying base

### AI Behaviors
- dependent on personality - opinion
- sex slave/simp (high infatuation, low aggression)
- begrudging sex slave (high infatuation, high aggression, higher terror than aggression)
    - will wait for opportunities when you are vulnerable to sexually attack you
- slave (low aggression, high terror)
- helper (low aggression)
- violent opponent (high aggression, low infatuation, not high terror)
- lecherous opponent (high aggression, high infatuation, higher infatuation than terror)
    
TODO describe more
        
### Attributes
- together with skills determine the impact of actions
- represents non-skill properties of an entity
- increasing these may have drawbacks
    - increasing STR will increasing muscle dimensions and decrease appeal for some
    
### Skills
- increasing these is always good
- improves with usage

### Combat Resources
- stamina per entity
    - max stamina depleted on orgasm
    - max stamina recover on long rest (clear stage)
    - (not sure) stamina deplete on special abilities / sprint
    - max stamina scales with CON
- vitality per Damageable body part
    - max for all parts at 100 (start with 100)
    - scales (linearly) with capability of associated actions
        - totally useless at 0 vitality
        - (not sure) destroy parts when vitality reaches 0
    - different rate of decrease for different parts to represent fragility
        - hit on the head will damage the head more than the same hit to the arm
- arousal per Arousable body part
    - max for all parts at 100 (start with 0)
    - different rates of increase for different parts to represent sensitivity
        - base sensitivity can also change (repeated use, piercings)
    - at 100 entity receives orgasm and can't take actions for some time

### Combat Mechanics
- separated into striking and wrestling
- (not sure) stance for preparing to avoid strikes or grapples
- striking
    - test for hit with actual geometry on top-down map
    - move displacement 
    - with high skill does way more damage than wrestling
    - higher risk of missing but higher reward than wrestling
    - some weapons allow cleaving (hit multiple units)
    - counter to high STR, low DEX entities
        - hit probability scales highly with DEX faster than damage scales with STR
- wrestling
    - all sexual actions occur in wrestling
        - GraspCapable parts can rub, InsertCapable parts can insert, LickCapable parts can lick
    - dramatically slows movement speed
    - grasp parts
    - escape wrestling by breaking grasp from all parts
        - each grasp break is a check with some RNG
        - high STR and high wrestling skill improves grasp and grasp defense 
    - once grasped, grasp again to pinch
        - pinch does unavoidable damage dependent on STR
    - counter to low STR, high DEX entities
- other
    - seduction actions
        - targets those that can see you
        - increases their lust opinion
        
## Planned polish
- transparency for hiding features (bushes, tree canopy) when you're under them
    - otherwise the hiding feature will be in the foreground blocking the avatars (allows for ambush by AI)
- resizing avatar to be the height of the screen and fit the game canvas to the rest on the right side
- more maps
- weapon type system?
    - blunt (scales best with strength)
    - pierce (scales somewhat with strength)
    - cut (doesn't scale with strength)
    
### Weapon definition
TODO