import {Point} from "../basetypes";
import {norm, diff} from "drawpoint/dist-esm";

export function roundToBase(num: number, base: number) {
    return Math.round(num / base) * base;
}

export function roundToBaseWithin(num: number, base: number, within: number) {
    const remainder = num % base;
    if (Math.abs(remainder) <= within || Math.abs(base - remainder) <= within) {
        return roundToBase(num, base);
    }
    return num;
}

export function roundPixel(xy: number) {
    return (0.5 + xy) << 0;
}

export function distance(x1: number, y1: number, x2: number, y2: number) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

export function distPoint(a: Point, b: Point): number {
    return norm(diff(a, b))
}

export function angleDiff(a: number, b: number) {
    let d = b - a;
    if (d > Math.PI) {
        d -= 2 * Math.PI;
    } else if (d < -Math.PI) {
        d += 2 * Math.PI;
    }
    return d;
}

export interface Rectangle {
    topLeft: Point,
    size: Point,
}

export function pointInRectangle(rect: Rectangle, pt: Point) {
    return pt.x >= rect.topLeft.x && pt.x <= rect.topLeft.x + rect.size.x && pt.y >= rect.topLeft.y && pt.y <= rect.topLeft.y + rect.size.y;
}