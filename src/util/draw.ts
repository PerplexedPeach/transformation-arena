import {extractBaseLocation, extractSideLocation} from "../../lib/dad/src/util/part";
import {Point} from "../basetypes";
import {hasMixin} from "ts-mixer";
import {DamageablePart} from "../mixins/damageable";
import {BaseEntity} from "../entity/player";

export const CM_TO_CANVAS = 0.35;

export const DEFAULT_PRINT_VITALITY_OPTIONS = {
    hideUndamagedParts: true
};

export function printPartVitality(this: typeof DEFAULT_PRINT_VITALITY_OPTIONS, canvas: HTMLCanvasElement,
                                  ctx: CanvasRenderingContext2D, config, avatar: BaseEntity) {
    // print vital bars with total width being maxVital / vitalLimit, and current width being vital / vitalLimit
    // use config.vitalColors to decide styling
    ctx.save();
    let i = Object.entries(avatar.maxVitals).length;
    const offsetX = 5;
    const barHeight = 5;
    const maxMaxWidth = 70 - 2 * offsetX;
    const color = "#c23b2d";
    const maxVitality = 100;
    const valueToWidth = 1 / maxVitality * maxMaxWidth;
    avatar.parts.forEach((part) => {
        if (!hasMixin(part, DamageablePart)) {
            return;
        }
        if (this.hideUndamagedParts && part.vitality === 100) {
            return;
        }

        ctx.strokeStyle = color;
        const offsetY = 30 + i * (barHeight + 2);

        // TODO display effect of Mods
        let width = maxVitality * valueToWidth;
        ctx.strokeRect(offsetX, offsetY, width, barHeight);

        ctx.font = "5px Arial";
        ctx.fillStyle = "#222";

        const baseLoc = extractBaseLocation(part.loc);
        const sideLoc = extractSideLocation(part.loc);
        ctx.fillText(sideLoc ? `${baseLoc} ${sideLoc[0]}`.toUpperCase() : baseLoc.toUpperCase(), offsetX + width + 2,
            offsetY + barHeight - 1);

        ctx.fillStyle = color;
        width = part.vitality * valueToWidth;
        ctx.fillRect(offsetX, offsetY, width, barHeight);
        i++;
    });
    ctx.restore();
}

export function drawPie(ctx: CanvasRenderingContext2D, pt: Point, radius: number, startAngle: number,
                        endAngle: number) {
    ctx.moveTo(pt.x, pt.y);
    ctx.arc(pt.x, pt.y, radius, startAngle, endAngle);
    ctx.lineTo(pt.x, pt.y);
}