import {Point, ScreenPoint} from "../basetypes";

export function getMovement(keys: object): Point | null {
    const dir = {
        y: 0,
        x: 0
    };
    if (keys['w']) {
        dir.y -= 1;
    }
    if (keys['s']) {
        dir.y += 1;
    }
    if (keys['a']) {
        dir.x -= 1;
    }
    if (keys['d']) {
        dir.x += 1;
    }
    if (keys['ArrowLeft']) {
        dir.x -= 1;
    }
    if (keys['ArrowRight']) {
        dir.x += 1;
    }
    if (keys['ArrowUp']) {
        dir.y -= 1;
    }
    if (keys['ArrowDown']) {
        dir.y += 1;
    }
    if (keys['Home']) {
        dir.y -= 1;
        dir.x -= 1;
    }
    if (keys['End']) {
        dir.y += 1;
        dir.x -= 1;
    }
    if (keys['PageUp']) {
        dir.y -= 1;
        dir.x += 1;
    }
    if (keys['PageDown']) {
        dir.y += 1;
        dir.x += 1;
    }

    // scale movement to be magnitude 1 in the selected direction
    const magnitude = Math.sqrt(dir.x * dir.x + dir.y * dir.y);
    if (magnitude === 0) {
        return null;
    }

    dir.x /= magnitude;
    dir.y /= magnitude;

    return dir;
}

export function truncateString(str: string, length: number): string {
    if (str.length <= length) {
        return str;
    }
    return str.slice(0, length - 2) + "..";
}

export const fps = {
    elapsed : 0,
    tick    : 0,
    lastText: null,
    init(displayId: string, game, interval: number) {
        const displayElem = document.getElementById(displayId);
        if (!displayElem) {
            throw new Error(`Can't find FPS display element with ID ${displayId}`);
        }
        setInterval(function () {
            let thisText = (this.tick / this.elapsed).toFixed(1);
            this.elapsed = this.tick = 0;
            if (game.paused) {
                thisText += " paused";
            }
            if (this.lastText !== thisText) {
                displayElem.textContent = thisText;
                this.lastText = thisText;
            }
        }.bind(this), interval);
    },
    update(delta: number) {
        this.tick += 1;
        this.elapsed += delta;
    },
};

export function randomChoice<T>(choices: T[]) {
    return choices[Math.floor(Math.random() * choices.length)];
}

export function getScreenSize(): ScreenPoint {
    return {
        x: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
        y: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
    };
}