import {
    ArmHuman,
    BodyPart,
    ButtHuman,
    ChestHuman,
    Clothing,
    FeetHuman,
    GroinHuman,
    HandHuman,
    HeadHuman,
    isParentPart,
    LegHuman,
    NeckHuman,
    NipplesHuman,
    Part, PenisHeadHuman, PenisHuman,
    Player, TesticlesHuman,
    TorsoHuman, VaginaHuman,
} from "../../lib/dad/src/index";
import {hasMixin, Mixin} from 'ts-mixer';

import {Moveable, MoveablePart} from "../mixins/moveable";
import {Actor} from "../mixins/actor";
import {DamageablePart} from "../mixins/damageable";
import {OngoingGrasp, Graspable, GraspCapable} from "../mixins/graspable";
import {Skeleton} from "../../lib/dad/src/skeletons/skeleton";
import {HitProperties, SpotStrike, StrikeCapable} from "../mixins/strikeable";
import {CM_TO_CANVAS} from "../util/draw";
import {GameMap} from "../map/map";
import {Entity, generateUID, neutralDefaultOpinion, Opinion, UID} from "./entity";
import {TopDown, TopDownLayer} from "../mixins/top_down";
import {Behavior} from "../behavior/behavior";
import {Sensor, SensorSlice} from "../mixins/sensor";
import {EntitySkills, scaleOnSkill, SkillSet} from "./skills";
import {Attribute, scaleOnAtt} from "./attributes";
import {Point} from "../basetypes";
import {Arousable} from "../mixins/arousable";
import {LickCapable} from "../mixins/lickable";
import {InsertCapable, InsertSize, Orifice} from "../mixins/insertable";

const STRIKE_EXTEND_FACTOR = 1;
const GRASP_DIST_FACTOR = 0.9;

class HeadExtended extends Mixin(HeadHuman, TopDown, DamageablePart, Arousable, LickCapable, Orifice) {
    constructor(...data) {
        super(...data);
        this.initDamageablePart(1, 0.3);
        this.initTopDown();
        this.initArousable(this.sensitivity || 0.3);
        this.initOrifice(this.depth || 10, this.girth || 5, this.elasticity || 10, this.stretch || 0, this.lubrication);
    }

    topDownLayer(): TopDownLayer {
        return TopDownLayer.HEAD;
    }

    baseOffset(avatar: Entity): Point {
        return {x: -3, y: 0};
    }

    renderTopDown(avatar, ctx, pos, colors) {

        if (avatar.dim.hairStyle > 0) {
            ctx.fillStyle = colors.hairFill;
            ctx.strokeStyle = colors.hairStroke;
        } else {
            ctx.fillStyle = colors.baseFill;
            ctx.strokeStyle = colors.baseStroke;
        }

        const head = avatar.dim.faceWidth * CM_TO_CANVAS * 0.4;
        const offset = this.offset(avatar);

        ctx.beginPath();
        ctx.ellipse(pos.x + offset.x, pos.y + offset.y, head, head, avatar.rot, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    }
}

class TorsoExtended extends Mixin(TorsoHuman, TopDown, DamageablePart) {
    constructor(...data) {
        super(...data);
        this.initDamageablePart(0.5, 0);
        this.initTopDown();
    }

    topDownLayer(): TopDownLayer {
        return TopDownLayer.TORSO;
    }

    baseOffset(avatar: Entity): Point {
        return {x: 0, y: 0};
    }

    renderTopDown(avatar, ctx, pos, colors) {
        const r = avatar.radius;
        ctx.fillStyle = colors.baseFill;
        ctx.strokeStyle = colors.baseStroke;
        ctx.beginPath();
        ctx.ellipse(pos.x, pos.y, r, r * 1.2, avatar.rot, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    }
}


class ArmExtended extends Mixin(ArmHuman, DamageablePart, Graspable) {
    constructor(...data) {
        super(...data);
        this.initDamageablePart(0.9, 0.1);
    }
}

class HandExtended extends Mixin(HandHuman, GraspCapable, StrikeCapable, TopDown) {
    constructor(...data) {
        super(...data);
        this.initStrikeCapable("punch", new SpotStrike(this.strikeRadius));
        this.initGraspCapable();
    }

    topDownLayer(): TopDownLayer {
        return TopDownLayer.HAND;
    }

    graspRange(avatar): number {
        return avatar.dim.armLength * CM_TO_CANVAS * GRASP_DIST_FACTOR;
    }

    graspStrength(avatar): number {
        // strength required to escape the grasp
        const baseStrength = scaleOnSkill(avatar, EntitySkills.WRESTLING, 8, 20);
        const strMod = scaleOnAtt(avatar, Attribute.STR, 1, 1.4);
        const dexMod = scaleOnAtt(avatar, Attribute.DEX, 1, 1.4); // technique
        return baseStrength * strMod * dexMod;
    }

    rubTerm(part: BodyPart): string {
        // TODO handle insertible -> "finger"
        return super.rubTerm(part);
    }

    strikeRange(avatar) {
        return avatar.dim.armLength * CM_TO_CANVAS * STRIKE_EXTEND_FACTOR;
    }

    strikeRadius(avatar) {
        return avatar.dim.handSize * CM_TO_CANVAS * 0.15;
    }

    strikeDuration(avatar): number {
        return scaleOnSkill(avatar, EntitySkills.STRIKING, 0.3, 0.15);
    }

    _calculateBaseHit(avatar): HitProperties {
        const baseProb = scaleOnSkill(avatar, EntitySkills.STRIKING, 0.4, 0.7);
        const dexProb = scaleOnAtt(avatar, Attribute.DEX, 0, 0.2);
        const baseDmg = scaleOnSkill(avatar, EntitySkills.STRIKING, 15, 30);
        const strDmg = scaleOnAtt(avatar, Attribute.STR, 1, 2);
        return {
            probability: baseProb + dexProb,
            damage     : baseDmg * strDmg
        };
    }

    baseOffset(avatar) {
        const r = avatar.radius;
        const offset = {x: r * 0.8, y: r * 0.6};
        // @ts-ignore
        if (this.side === Part.LEFT) {
            offset.y = -offset.y;
        }
        return offset;
    }

    renderTopDown(avatar, ctx, pos, colors) {
        ctx.fillStyle = colors.baseFill;
        ctx.strokeStyle = colors.baseStroke;

        const offset = this.offset(avatar);
        const hand = this.strikeRadius(avatar);

        ctx.beginPath();
        ctx.ellipse(pos.x + offset.x, pos.y + offset.y, hand, hand, avatar.rot, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    }
}

class LegExtended extends Mixin(LegHuman, DamageablePart) {
    constructor(...data) {
        super(...data);
        this.initDamageablePart(0.8, 0.1);
    }
}

class FeetExtended extends Mixin(FeetHuman, StrikeCapable, TopDown, MoveablePart) {
    constructor(...data) {
        super(...data);
        this.initStrikeCapable("kick", new SpotStrike(this.strikeRadius));
        this.initMoveablePart(100);
    }

    topDownLayer(): TopDownLayer {
        return TopDownLayer.FEET;
    }

    baseOffset(avatar) {
        const r = avatar.radius;
        const offset = {x: r * 0.4, y: r * 0.6};
        // @ts-ignore
        if (this.side === Part.LEFT) {
            offset.y = -offset.y;
        }
        return offset;
    }

    strikeRange(avatar) {
        return avatar.dim.legLength * CM_TO_CANVAS * STRIKE_EXTEND_FACTOR;
    }

    strikeRadius(avatar) {
        return avatar.dim.feetLength * CM_TO_CANVAS * 0.1;
    }

    strikeDuration(avatar): number {
        return scaleOnSkill(avatar, EntitySkills.KICKING, 0.5, 0.3) *
            scaleOnSkill(avatar, EntitySkills.STRIKING, 1, 0.8);
    }

    _calculateBaseHit(avatar): HitProperties {
        const baseProb = scaleOnSkill(avatar, EntitySkills.KICKING, 0.3, 0.7);
        const dexProb = scaleOnAtt(avatar, Attribute.DEX, 0, 0.2);
        const baseDmg = scaleOnSkill(avatar, EntitySkills.KICKING, 25, 40);
        const strDmg = scaleOnAtt(avatar, Attribute.STR, 1, 2);
        return {
            probability: baseProb + dexProb,
            damage     : baseDmg * strDmg
        };
    }

    renderTopDown(avatar, ctx, pos, colors) {
        ctx.fillStyle = colors.baseFill;
        ctx.strokeStyle = colors.baseStroke;

        const offset = this.offset(avatar);

        ctx.beginPath();
        ctx.ellipse(pos.x + offset.x, pos.y + offset.y, this.strikeRadius(avatar),
            avatar.dim.feetWidth * CM_TO_CANVAS * 0.3,
            avatar.rot, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    }
}

class ButtExtended extends Mixin(ButtHuman, Arousable, Graspable, Orifice) {
    constructor(...data) {
        super(...data);
        this.initArousable(this.sensitivity || 0.2);
        this.initOrifice(this.depth || 10, this.girth || 0.2, this.elasticity || 2, this.stretch || 0, this.lubrication);
    }
}

// TODO make damageable type with base scaling dependent on breast size and sensitivity
class ChestExtended extends Mixin(ChestHuman, Arousable) {
    constructor(...data) {
        super(...data);
        this.initArousable(this.sensitivity || 0.5);
    }
}

class VaginaExtended extends Mixin(VaginaHuman, Arousable, Orifice) {
    constructor(...data) {
        super(...data);
        this.initArousable(this.sensitivity || 1.0);
        this.initOrifice(this.depth || 20, this.girth || 2, this.elasticity || 6, this.stretch || 0, this.lubrication);
    }
}

// TODO make damageable dependent on sensitivity
class TesticlesExtended extends Mixin(TesticlesHuman, Arousable) {
    constructor(...data) {
        super(...data);
        this.initArousable(this.sensitivity || 0.5);
    }
}

export class PenisExtended extends Mixin(PenisHuman, Arousable, InsertCapable) {
    constructor(...data) {
        super(...data);
        this.initArousable(this.sensitivity || 1.0);
    }

    insertSize(avatar): InsertSize {
        const length = avatar.dim.penisSize / 10;
        const girth = avatar.dim.penisSize / 40 + avatar.Mods.penisThickness / 10;
        return {length, girth};
    }
}

Skeleton.human.maleParts = [
    {
        partGroup: "parts",
        side     : null,
        part     : TesticlesExtended,
    },
    {
        partGroup: "parts",
        side     : null,
        part     : PenisExtended,
    },
    {
        partGroup: "decorativeParts",
        side     : null,
        part     : PenisHeadHuman,
    },
];

Skeleton.human.femaleParts = [
    {
        partGroup: "parts",
        side     : null,
        part     : VaginaExtended,
    },
];
Skeleton.human.defaultParts = [
    {
        side: null,
        part: HeadExtended
    },
    {
        side: null,
        part: NeckHuman
    },
    {
        side: Part.LEFT,
        part: ArmExtended
    },
    {
        side: Part.RIGHT,
        part: ArmExtended
    },
    {
        side: Part.LEFT,
        part: HandExtended
    },
    {
        side: Part.RIGHT,
        part: HandExtended
    },
    {
        side: null,
        part: TorsoExtended
    },
    {
        side: Part.LEFT,
        part: LegExtended
    },
    {
        side: Part.RIGHT,
        part: LegExtended
    },
    {
        side: Part.LEFT,
        part: FeetExtended
    },
    {
        side: Part.RIGHT,
        part: FeetExtended
    },
    {
        side: null,
        part: GroinHuman
    },
    {
        side: null,
        part: ButtExtended
    },
    {
        side: null,
        part: ChestExtended
    },
    {
        side: null,
        part: NipplesHuman
    },
];

interface EntityConstructionData {
    map: GameMap,
    behaviors?: Behavior[],
    defaultNewOpinion?: typeof neutralDefaultOpinion,
    damageListener?: typeof DamageablePart.prototype.damageListener
    arousalListener?: typeof Arousable.prototype.arousalListener
}

// TODO add more types that can be added to inventory
type InventoryStorable = Clothing;

export class BaseEntity extends Mixin(Player, Actor, Moveable, Sensor) implements Entity {
    public readonly uid: UID;
    public name: string;
    public inv: InventoryStorable[];
    public skills: SkillSet;
    public wrestling: OngoingGrasp[];

    private _opinionMemory: Record<UID, Opinion> = {};
    private readonly _defaultNewOpinion: typeof neutralDefaultOpinion;


    // TODO for now we just ignore Player's properties since they are not typescript
    constructor(playerData: any, data: EntityConstructionData) {
        super(playerData);

        if (!this.uid) {
            this.uid = (playerData.name ? playerData.name : "") + generateUID();
        }
        if (!this.inv) {
            this.inv = [];
        }
        if (!this._defaultNewOpinion) {
            this._defaultNewOpinion = data.defaultNewOpinion || neutralDefaultOpinion;
        }
        if (!this.skills) {
            this.skills = {};
        }
        this.wrestling = [];

        // TODO better way of handling the pleasure vital? It builds up rather than counts down as expected; 100 is the default
        if (this.vitals.pleasure === 100) {
            this.vitals.pleasure = 0;
        }

        this.initActor(data.behaviors);
        this.initMoveable(data.map, this.radius, playerData.x, playerData.y, playerData.rot, this.moveSpeed,
            playerData.ignoreMoveable || false);

        if (data.damageListener) {
            this.partsWithMixin(DamageablePart).map(part => part.addDamageListener(data.damageListener));
        }
        if (data.arousalListener) {
            this.partsWithMixin(Arousable).map(part => part.addArousalListener(data.arousalListener));
        }
    }

    desc() {
        // TODO describe entity
        return "";
    }

    damageable(): boolean {
        for (let part of this.parts) {
            if (hasMixin(part, DamageablePart) && part.vitality > 0) {
                return true;
            }
        }
        return false;
    }

    graspable(): boolean {
        return this.partsWithMixin(Graspable).length > 0;
    }

    partsWithMixin<T>(mixin: new (...args: any[]) => T) {
        const parts: Array<T & BodyPart> = [];
        for (let part of this.parts) {
            if (hasMixin(part, mixin)) {
                parts.push(part);
            }
        }
        return parts;
    }

    partIsWrestling(part: BodyPart) {
        if (hasMixin(part, GraspCapable) || hasMixin(part, Graspable)) {
            // @ts-ignore
            return !!this.wrestling.find(grasp => grasp.grasped === part || grasp.grasper === part);
        }
        return false;
    }

    parentPart(part: BodyPart): BodyPart | null {
        if (!part.parentPart) {
            return null;
        }
        for (let parentCandidate of this.parts) {
            if (isParentPart(parentCandidate, part)) {
                return parentCandidate;
            }
        }
        return null;
    }

    get moveSpeed() {
        const moveParts = this.partsWithMixin(MoveablePart);
        const totalSpeed = moveParts.reduce((speed, part) => speed + part.moveSpeed(this), 0);
        // when wrestling, can't move
        const wrestleFactor = (this.wrestling.length === 0) ? 1 : 0.02;
        return totalSpeed * wrestleFactor;
    }

    get radius() {
        // @ts-ignore
        return this.dim.shoulderWidth * CM_TO_CANVAS * 0.85;
    }

    sensingSlices(): SensorSlice[] {
        // TODO take into account how many eyes and what type of eyes we have, or directly query parts for this
        // some senses are directional and some are not (kinesthetic sense)
        return [{radius: 200, halfArc: Math.PI / 4}, {radius: 100, halfArc: Math.PI}];
    }

    opinion(other: Entity): Opinion {
        if (!(other.uid in this._opinionMemory)) {
            this._opinionMemory[other.uid] = this._defaultNewOpinion(other);
        }
        const opinion = this._opinionMemory[other.uid];
        // TODO calculate and apply opinion modifiers

        return opinion;
    }

    adjustBaseOpinion(other: Entity, opinionDelta: Partial<Opinion>) {
        if (!(other.uid in this._opinionMemory)) {
            this._opinionMemory[other.uid] = this._defaultNewOpinion(other);
        }
        const opinion = this._opinionMemory[other.uid];
        for (let [k, delta] of Object.entries(opinionDelta)) {
            opinion[k] += delta;
        }
    }
}
