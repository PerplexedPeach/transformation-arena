// TODO extend Player stats, vitals, etc here
import {vitalLimits} from "../../lib/dad/src/player/vitals";
import {statLimits} from "../../lib/dad/src/player/stats";
import {extendDimensionCalc} from "../../lib/dad/src/player/dimensions";

delete vitalLimits.sanity;

const MAX_ATT_VALUE = 20;
const AVG_ATT_VALUE = 8;
const MIN_ATT_VALUE = 0;

// const attributes = ['str', 'dex', 'con', 'something'] as const;
// export type Attribute = (typeof attributes)[number];

export enum Attribute {
    STR = "str",
    DEX = "dex",
    CON = "con"
}

statLimits[Attribute.STR] = {
    low  : MIN_ATT_VALUE,
    high : MAX_ATT_VALUE,
    avg  : AVG_ATT_VALUE,
    stdev: 2,
    bias : -2
};
statLimits[Attribute.DEX] = {
    low  : MIN_ATT_VALUE,
    high : MAX_ATT_VALUE,
    avg  : AVG_ATT_VALUE,
    stdev: 2,
    bias : 2
};
statLimits[Attribute.CON] = {
    low  : MIN_ATT_VALUE,
    high : MAX_ATT_VALUE,
    avg  : AVG_ATT_VALUE,
    stdev: 2,
    bias : -1
};

// @ts-ignore
extendDimensionCalc("human.upperMuscle", function (base) {
    return base + this.diff("str") * 1.5 - this.diff("dex") * 0.4;
});
// @ts-ignore
extendDimensionCalc("human.lowerMuscle", function (base) {
    return base + this.diff("str") - this.diff("dex") * 0.4;
});
// @ts-ignore
extendDimensionCalc("human.armThickness", function (base) {
    return base + this.diff("str") - this.diff("dex") * 0.5;
});
// @ts-ignore
extendDimensionCalc("human.legFullness", function (base) {
    return base - this.diff("dex") * 0.3;
});
// @ts-ignore
extendDimensionCalc("human.buttFullness", function (base) {
    return base - this.diff("dex") * 0.3;
});
console.log('extending stats');

export function scaleOnAtt(avatar, att: Attribute, avgValue: number, maxValue: number) {
    // linear scaling
    return avgValue + (maxValue - avgValue) * ((avatar[att] || AVG_ATT_VALUE) - AVG_ATT_VALUE) / (MAX_ATT_VALUE - AVG_ATT_VALUE);
}