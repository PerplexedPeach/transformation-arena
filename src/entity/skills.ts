export enum EntitySkills {
    STRIKING,
    KICKING,
    WRESTLING,
    RUBBING,
    LICKING,
    FUCKING
}

// TODO map text to these values (Not, Dabbling, Novice, Adequate, Competent, ...)
// TODO calculate xp levels (exponential)
export type SkillValue = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export type SkillSet = Partial<Record<EntitySkills, SkillValue>>;

export function scaleOnSkill(avatar, skill: EntitySkills, nonValue: number, legendaryValue: number) {
    // linear scaling
    return nonValue + (legendaryValue - nonValue) * (avatar.skills[skill] || 0) / 10;
}