import {Point} from "../basetypes";
import {BodyPart} from "../../lib/dad/src";
import {GameMap} from "../map/map";
import {OngoingGrasp} from "../mixins/graspable";

export type OpinionValue = -10 | -9 | -8 | -7 | -6 | -5 | -4 | -3 | -2 | -1 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

// TODO map each disposition value to text
export interface Opinion {
    // how much they respect your power (negative means they are confident against you; high value is fear)
    awe: OpinionValue;
    // how much they dislike you (negative is love)
    hate: OpinionValue;
    // how much they are attracted to you (negative is disgust)
    lust: OpinionValue;
}

export function neutralDefaultOpinion(entity: Entity): Opinion {
    return {awe: 0, hate: 0, lust: 0};
}


export type UID = string;

export function generateUID() {
    return Math.random().toString(36).substr(2, 7);
}

export interface Entity extends Point {
    uid: UID;
    name?: string;
    parts: BodyPart[];
    radius: number;
    map: GameMap;
    wrestling: OngoingGrasp[];

    opinion(other: Entity): Opinion;
    damageable(): boolean;
    graspable(): boolean;
}

export function isEntity(obj: any): obj is Entity {
    return 'uid' in obj;
}
