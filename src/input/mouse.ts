import {GameClass} from "../base";
import {ScreenPoint} from "../basetypes";

export const Mouse = {
    // button types corresponding to mouseEvent.button
    LEFT: 0,
    MIDDLE: 1,
    RIGHT: 2,
};

export function listenForEvents(game: GameClass) {
    document.addEventListener("mousemove", function (e) {
        if (game.state) {
            game.state.handleMouseMove(game, e);
        }
    }, false);
    document.addEventListener("mouseup", function (e) {
        if (game.state) {
            game.state.handleMouseUp(game, e);
        }
    }, false);
    document.addEventListener("contextmenu", function (e) {
        handleContextMenu(game, e);
    }, false);
}

export function getClickScreenPosition(game:GameClass, e): ScreenPoint {
    // const screenX = e.offsetX || e.layerX;
    // const screenY = e.offsetY || e.layerY;
    const screenX = e.pageX - game.avatarWidth;
    const screenY = e.pageY;
    return {
        x: screenX,
        y: screenY
    };
}

function handleContextMenu(game: GameClass, e) {
    e.preventDefault();
    game.state.handleContextMenu(game, getClickScreenPosition(game, e));
    return false;
}
