import {GameClass} from "../base";

export type Keys = {
    [keyName: string]: boolean;
}
const keys: Keys = {};

export function listenForEvents(game: GameClass) {
    document.addEventListener("keydown", function (e) {
        keyDownHandler(game, e);
    }, false);
    document.addEventListener("keyup", function (e) {
        keyUpHandler(game, e);
    }, false);
}

function keyDownHandler(game: GameClass, e) {
    if (e.key === "F5" || e.key === "F12") {
        return;
    }
    // prevent browser navigation shortcuts
    e.preventDefault();
    console.log(e.key);
    keys[e.key] = true;
    // let the current state deal with currently pressed keys
    // this inside the handler will be the game
    game.state.handleKeyDown(game, keys);
}

function keyUpHandler(game: GameClass, e) {
    e.preventDefault();
    keys[e.key] = false;
    game.state.handleKeyUp(game, keys);
}
