import {BaseEntity, PenisExtended} from "../entity/player";
import {GameMap} from "../map/map";
import {Follower} from "../behavior/follower";
import {GameClass} from "../base";
import {TubeTopSleeves} from "../../lib/dad/src/clothes/tops";
import {ItemRarity} from "../menu/items";
import {LacedLeggins} from "../../lib/dad/src/clothes/super_pants";
import {getPattern} from "../../lib/dad/src/util/pattern";
import {SimpleBelt} from "../../lib/dad/src/clothes/accessory";
import {Lipstick} from "../../lib/dad/src/clothes/lipstick";
import {Nails} from "../../lib/dad/src/clothes/nails";
import {CrossedChoker} from "../../lib/dad/src/clothes/choker";
import {SimpleChain} from "../../lib/dad/src/clothes/necklaces";
import {LocalMovement} from "../state/local_movement";
import {mapGenerator} from "./map_generator";
import {Engagement} from "../behavior/engagement";
import {Part} from "../../lib/dad/src/parts/part";


type Level = (game: GameClass, prevPlayer?: BaseEntity, prevMap?: GameMap) => { player: BaseEntity };

export const levelLoader: Record<number, Level> = {};

levelLoader[0] = function (game: GameClass) {
    // ignore previous player since we are creating the player
    // TODO pass tiles in
    const map = new GameMap(mapGenerator[0]());
    const player = initPlayer(game, map);
    map.addEntity(player);

    const follower = new BaseEntity({
        name: "follower1",
        x   : 300, y: 300
    }, {
        map: map, behaviors: [new Follower(player, 50)]
    });
    map.addEntity(follower);

    // const coward = new BaseEntity({
    //     name   : "coward",
    //     x      : 300, y: 250, rot: -Math.PI * 0.8,
    //     basedim: {
    //         skin: 14
    //     }
    // }, {
    //     map      : map,
    //     behaviors: [new Engagement(300, {confidence: -10, composure: 0, modesty: 0})]
    // });
    // map.addEntity(coward);

    const angry = new BaseEntity({
        name   : "angry",
        x      : 200, y: 200, rot: Math.PI * 0.8,
        basedim: {
            skin: 18
        }
    }, {
        map      : map,
        behaviors: [new Engagement(300, {confidence: 5, composure: -6, modesty: 0})]
    });
    map.addEntity(angry);

    LocalMovement.init(game, player, map);

    return {player};
};


function initPlayer(game: GameClass, map: GameMap) {
    const PC = new BaseEntity({
        name   : "MC",
        x      : 100,
        y      : 100,
        rot    : -Math.PI / 4,
        fem    : 11,
        basedim: {
            areolaSize     : 14.923766816143496,
            armThickness   : 58.468958260259555,
            armLength      : 45,
            bellyProtrusion: 0,
            breastSize     : 9.974887892376682,
            buttFullness   : 13.019992984917572,
            chinWidth      : 63.335671694142405,
            eyelashLength  : 4,
            eyeSize        : 13.019992984917572,
            faceFem        : 40,
            faceLength     : 212.32549982462294,
            faceWidth      : 82.74465099964925,
            hairLength     : 39.14618834080717,
            hairStyle      : 4,
            hairHue        : 0,
            hairSaturation : 8.035874439461884,
            hairLightness  : 33.2914798206278,
            handSize       : 118.9757979656261,
            height         : 163.65022421524662,
            hipWidth       : 110.85584005612066,
            legFem         : 39.95790950543669,
            legFullness    : 4.489652753419852,
            legLength      : 98.79340582251841,
            lipSize        : 18.85654156436338,
            lowerMuscle    : 22.448263767099263,
            neckLength     : 72.73237460540162,
            neckWidth      : 39.489652753419854,
            penisSize      : 50,
            shoulderWidth  : 64.28699551569507,
            skin           : -1.9291476674850934,
            testicleSize   : 60,
            upperMuscle    : 0,
            vaginaSize     : 40,
            waistWidth     : 102.32549982462294,
        },

        Mods: {
            armRotation            : 0.4089686098654681,
            arousal                : 0,
            breastPerkiness        : 3.805682216766048,
            browBotCurl            : -1.6771300448430502,
            browTopCurl            : 6.358744394618835,
            browCloseness          : 0.7910313901345294,
            browHeight             : 1.2502242152466359,
            browLength             : -5.408071748878924,
            browSharpness          : -1.0457399103139018,
            browThickness          : -1.9641255605381165,
            browOutBias            : 0.5614349775784753,
            browTilt               : 2.041255605381167,
            cheekFullness          : 0.20343739038933606,
            chinLength             : -3,
            earlobeLength          : -0.45106980007015096,
            eyeBias                : 2.6,
            eyeCloseness           : 28.573991031390136,
            eyeBotBias             : -3.3417040358744394,
            eyeBotSize             : 13.980269058295967,
            eyeHeight              : -6.9641255605381165,
            eyeTilt                : 6.977578475336323,
            eyeTopSize             : -1.3212556053811646,
            eyeWidth               : -0.8161434977578477,
            eyelashBias            : 5.826025955804981,
            eyelashAngle           : 0.42,
            eyelidBias             : -0.7962118554893021,
            eyelidHeight           : -1.555243774114346,
            hairAccessoryHue       : 202.03437390389334,
            hairAccessorySaturation: 59,
            hairAccessoryLightness : 43,
            irisHeight             : -1.275336322869956,
            irisHue                : 70.25650224215246,
            irisSaturation         : 27.55156950672646,
            irisLightness          : 67.73094170403587,
            irisSize               : 15.497757847533633,
            jawJut                 : -0.1227639424763236,
            limbalRingSize         : 25.255605381165918,
            lipBias                : 28.062780269058294,
            lipCupidsBow           : -7.47264573991032,
            lipCurl                : -4.387443946188341,
            lipHeight              : 0.3318385650224229,
            lipTopCurve            : -17.856502242152466,
            lipTopSize             : -8.060986547085204,
            lipBotSize             : 25.38475336322871,
            lipWidth               : -153.5695067264574,
            neckCurve              : -6.531041739740441,
            noseHeight             : -4.030493273542602,
            noseLength             : 41.78654708520179,
            noseRidgeHeight        : 0.3318385650224229,
            noseRoundness          : 4.83695067264574,
            noseWidth              : 18.724663677130046,
            nostrilSize            : 10.165919282511211,
            pupilSize              : 13.468958260259559,
            shoeHeight             : 3,
            skinHue                : -0.2547085201793706,
            skinSaturation         : -4.846636771300448,
            skinLightness          : 4.337219730941705,
        },
    }, {
        map            : map,
        damageListener : () => game.focusNeedsRedraw = true,
        arousalListener: () => game.focusNeedsRedraw = true,
    });

    // test insert action
    // @ts-ignore
    const myPart = Part.create(PenisExtended);
    PC.attachPart(myPart);

    const tubeTop = new TubeTopSleeves({
        fill         : "hsla(335.5,43.0%,55.3%,1.00)",
        armLoose     : 0.5446511149933203,
        chestCoverage: 0.31024560682355373,
        waistCoverage: 0.7837837837837838,
        rarity       : ItemRarity.RARE,
        name         : "Breezy Top"
        // fill         : da.getPattern("lace", 100)
    });

    PC.wearClothing(tubeTop);

    const lacedLeggings = new LacedLeggins({
        fill  : getPattern("brown leather", 150),
        rarity: ItemRarity.UNCOMMON,
    });

    PC.wearClothing(lacedLeggings);

    const belt = new SimpleBelt({
        beltCurve    : -3.2000000000000015,
        beltWidth    : 3.257630253827976,
        thickness    : 1.0999999999999992,
        waistCoverage: 0.941629842770527,
        fill         : getPattern("kimono flowers", 100),
        rarity       : ItemRarity.LEGENDARY,
        name         : "Cinch of Speed"
    });

    PC.wearClothing(belt);

    const makeupColor = "hsl(328, 39%, 50%)";
    const lipStick = new Lipstick({
        fill: makeupColor,
        name: "some really long string that should be wrapped; is this filling up the screen enough yet? a b c d e f g h i j k l m n o p q r s t"
    });
    PC.wearClothing(lipStick);
    const nails = new Nails({
        fill: makeupColor,
    });
    PC.wearClothing(nails);

    const choker = new CrossedChoker();
    PC.wearClothing(choker);

    const chainNecklace = new SimpleChain({
        cleavageCoverage: 0.1523995478368103,
        dash            : 4.3999999999999995,
    });

    PC.wearClothing(chainNecklace);
    return PC;
}