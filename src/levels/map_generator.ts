import {MapTiles, Tile} from "../map/map";
import {RNG, Map} from "rot-js";

export const mapGenerator: Record<number, () => MapTiles> = {};

function create2DArray(rows, columns) {
    const x = new Array(rows);
    for (let i = 0; i < rows; i++) {
        x[i] = new Array(columns);
    }
    return x;
}

mapGenerator[0] = function () {
    const mapSize = {x: 30, y: 20};
    const map = create2DArray(mapSize.y, mapSize.x);
    // RNG.setSeed(123);
    const mapGen = new Map.Arena(mapSize.y, mapSize.x);
    mapGen.create((x, y, wall) => {
        map[x][y] = (wall) ? Tile.TREE : Tile.FLOOR;
    });
    return map;
    // static for now
    // return [
    //     [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
    //     [3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3],
    //     [3, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 3],
    //     [3, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 3],
    //     [3, 3, 3, 1, 1, 2, 3, 3, 3, 3, 3, 3]
    // ];
};