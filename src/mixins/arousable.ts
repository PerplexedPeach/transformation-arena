import {clamp} from "drawpoint/dist-esm";
import {Orgasm} from "../action/sexual";
import {BaseEntity} from "../entity/player";
import {Entity} from "../entity/entity";

const STA_DMG_PER_ORGASM = 5;
const BASE_ORGASM_DURATION = 5;
const SENSITIVITY_INCREASE = 1.1;
const BASE_PLEASURE_PER_ACTION = 2;

export class Arousable {
    arousal: number;
    sensitivity: number = 1;
    // TODO min and max arousal of a part can be modified as part of traits
    minArousal: number = 0;
    maxArousal: number = 100;
    arousalListener: (Arousable) => void;

    initArousable(sensitivity: number) {
        this.arousal = this.minArousal;
        this.sensitivity = sensitivity;
    }

    addArousalListener(listener) {
        this.arousalListener = listener;
    }

    arouse(avatar: Entity, arousal: number) {
        let pleasure = 0;
        if (arousal > 0) {
            arousal = this.sensitivity * arousal;
            this.arousal = clamp(this.arousal + arousal, this.minArousal, this.maxArousal);
            // take pleasure damage based on current arousal of this part
            pleasure = this.arousal / 100 * BASE_PLEASURE_PER_ACTION;
            // TODO modify pleasure based on traits
            this.takePleasure(avatar as BaseEntity, pleasure);
            if (this.arousalListener) {
                this.arousalListener(this);
            }
        }
        return [arousal, pleasure];
    }

    takePleasure(avatar: BaseEntity, pleasure: number) {
        avatar.vitals.pleasure += pleasure;
        if (avatar.vitals.pleasure >= avatar.maxVitals.pleasure) {
            console.log("orgasm");
            // reset pleasure
            avatar.vitals.pleasure = 0;
            // TODO attach a skill to this?
            avatar.overrideAction(new Orgasm(avatar, this, BASE_ORGASM_DURATION));
            // decrease avatar max stamina
            avatar.Mods.stamina -= STA_DMG_PER_ORGASM;
            // TODO other effects from orgasming from this part; for example gaining traits
            // increase part sensitivity
            this.sensitivity *= SENSITIVITY_INCREASE;
        }
    }
}

