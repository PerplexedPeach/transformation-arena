import {add, clamp} from "drawpoint/dist-esm";
import {Entity} from "../entity/entity";
import {DamageablePart} from "./damageable";
import {distPoint} from "../util/math";
import {hasMixin} from "ts-mixer";
import {TopDown} from "./top_down";

export interface StrikeShape {
    hitsEntity(strikerParent: Entity, striker: StrikeCapable, target: Entity, targetPart?: DamageablePart): boolean;
}

export class SpotStrike implements StrikeShape {
    constructor(private radiusGetter: (Entity) => number) {
    }

    hitsEntity(strikerParent: Entity, striker: StrikeCapable, target: Entity, targetPart?: DamageablePart): boolean {
        const strikeRadius = this.radiusGetter(strikerParent);
        const strikePos = add(strikerParent, striker.offset(strikerParent));
        // TODO implement hitting based on target part location rather than just the base entity location
        // intersection of two circles
        return distPoint(strikePos, target) < strikeRadius + target.radius;
    }
}

export interface HitProperties {
    probability: number;
    damage: number;
}

// @ts-ignore
export class StrikeCapable extends TopDown {
    public strikeTerm: string;
    public strikeShape: StrikeShape;

    initStrikeCapable(strikeTerm: string, strikeShape: StrikeShape) {
        this.initTopDown();
        this.strikeTerm = strikeTerm;
        this.strikeShape = strikeShape;
    }

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract strikeRange(avatar): number;

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract strikeDuration(avatar): number;

    calculateHit(avatar, target: Entity, targetPart: DamageablePart): HitProperties {
        const baseHit = this._calculateBaseHit(avatar);
        let vitalityScale = 1;
        if (hasMixin(this, DamageablePart)) {
            vitalityScale = this.vitality / 100;
        }
        // TODO add RNG for chance hits
        // TODO consider armor worn at that target part
        return {
            probability: clamp((baseHit.probability - targetPart.baseHitDifficulty) * vitalityScale, 0.01, 0.95),
            damage     : baseHit.damage * vitalityScale
        };
    }

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract _calculateBaseHit(avatar): HitProperties;

}