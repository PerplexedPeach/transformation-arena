import {Point} from "../basetypes";
import {Entity} from "../entity/entity";
import {add, rotatePoints} from "drawpoint/dist-esm";

export type ColorMap = { [name: string]: string };

export enum TopDownLayer {
    HEAD,
    HAND,
    TORSO,
    FEET
}

export class TopDown {
    public displacement: Point;

    initTopDown() {
        this.displacement = {x: 0, y: 0};
    }

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract baseOffset(avatar: Entity): Point;

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract renderTopDown(avatar: Entity, ctx: CanvasRenderingContext2D, pos: Point, colors: ColorMap): void;

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract topDownLayer(): TopDownLayer;

    /**
     * Require that all users of this mixin has baseOffset(avatar) defined
     * @param avatar
     * @returns {{x: *, y: *}}
     */
    offset(avatar): Point {
        const pt = add(this.baseOffset(avatar), this.displacement);
        rotatePoints({x: 0, y: 0}, avatar.rot, pt);
        return pt;
    }
}
