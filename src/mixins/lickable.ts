import {EntitySkills, scaleOnSkill} from "../entity/skills";

const BASE_LICK_AROUSAL = 10;

export class LickCapable {
    lickArousal(avatar) {
        return scaleOnSkill(avatar, EntitySkills.LICKING, 1, 2) * BASE_LICK_AROUSAL;
    }
    lickRange() {
        return 5;
    }
}

