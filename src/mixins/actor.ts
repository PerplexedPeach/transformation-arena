import {Action, ActionState} from "../action/action";
import {Behavior} from "../behavior/behavior";

export class Actor {
    // every act-able entity will have an action stack they'll want to do
    // for now attribute no priority to each action (it'll just be a stack)
    // most recent needs override previous ones
    public todo: Action[];
    public _behaviors: Behavior[];

    initActor(behaviors?: Behavior[]) {
        this.todo = [];
        if (behaviors) {
            this._behaviors = behaviors;
        } else {
            this._behaviors = [];
        }
    }

    queueAction(action: Action): void {
        this.todo.push(action);
    }

    overrideAction(action: Action) {
        this.todo.unshift(action);
    }

    getUpcomingActionOfType<T extends new (...args: any[]) => Action>(actionType: T) {
        return this.todo.find(action => action instanceof actionType) as InstanceType<T> | undefined;
    }

    canTakeAction(): boolean {
        return this.todo.length > 0;
    }

    takeAction(delta: number) {
        // where we can put "AI" by overriding this in an Actor mixin subclass
        // first behaviors are higher priority
        for (let behavior of this._behaviors) {
            // break on first modification to avoid lower priority behaviors overriding
            if (behavior.modifyActions(this, delta)) {
                break;
            }
        }

        let hadEffect = false;
        let finishedAction = false;
        if (!this.canTakeAction()) {
            return [hadEffect, finishedAction];
        }
        // TODO some actions can be completed simultaneously?
        hadEffect = this.todo[0].act(delta);
        if (this.todo[0].state === ActionState.FINISHED) {
            this.todo.shift();
            finishedAction = true;
        }
        return [hadEffect, finishedAction];
    }
}

