import {roundToBaseWithin} from "../util/math";
import {add, clamp} from "drawpoint/dist-esm";
import {Point, Pose} from "../basetypes";
import {GameMap} from "../map/map";
import {DamageablePart} from "./damageable";
import {hasMixin} from "ts-mixer";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {BaseEntity} from "../entity/player";


export class Moveable implements Pose {
    public radius: number;
    public moveSpeed: number;
    public moveListener: ((this: any, moved: boolean) => void) | null;
    public map: GameMap;
    public x: number;
    public y: number;
    public rot: number;
    public ignorePassability: boolean;

    initMoveable(map: GameMap, radius, x, y, rot, speed, ignorePassability) {
        // how many pixels per tick
        // external functions to be called whenever the actor makes a move
        this.moveListener = null;
        // not defined elsewhere
        if (!this.radius) {
            this.radius = radius;
        }
        if (!this.moveSpeed) {
            this.moveSpeed = speed;
        }
        this.map = map;
        this.x = x;
        this.y = y;
        this.rot = rot; // rotation
        this.ignorePassability = ignorePassability;
    }

    addMoveListener(listener): void {
        // for now just allow 1 listener
        this.moveListener = listener;
    }

    move(delta: number, dir: Point): boolean {
        const movedXY = add(this, dir, this.moveSpeed * delta);

        const collision = (this.ignorePassability) ? false : this.collide(movedXY);
        let roundedPosition = false;

        if (!collision) {
            const maxXY = this.map.tilesToPixels({x: this.map.cols, y: this.map.rows});
            this.x = clamp(movedXY.x, 0, maxXY.x);
            this.y = clamp(movedXY.y, 0, maxXY.y);
        } else {
            // consider rounding position to be flush against the colliding tile
            let curPos = [this.x, this.y];
            this.x = this._moveDim(dir.x, this.x, this.radius, this.map.tsize);
            this.y = this._moveDim(dir.y, this.y, this.radius, this.map.tsize);
            roundedPosition = (curPos[0] !== this.x) || (curPos[1] !== this.y);
        }

        const moved = !collision || roundedPosition;
        if (this.moveListener) {
            this.moveListener(moved);
        }
        return moved;
    }

    _moveDim(dir: number, dim: number, offset: number, maxDim: number): number {
        const flush = ((dim - offset) % maxDim) === 0;
        if (!flush && dir !== 0) {
            if (dir > 0) {
                return roundToBaseWithin(dim + offset, maxDim, 5) - offset;
            } else {
                return roundToBaseWithin(dim - offset, maxDim, 5) + offset;
            }
        }
        return dim;
    }

    /**
     * return whether a collision occurs at a certain location
     */
    collide(point: Point): boolean {
        // TODO handle radius of this location all tiles within radius of this location
        return !this.map.isPassable(this.map.pixelsToTiles(point));
        // // -1 in right and bottom is because image ranges from 0..63
        // // and not up to 64
        // const left = point.x - this.radius;
        // const right = point.x + this.radius - 1;
        // const top = point.y - this.radius;
        // const bottom = point.y + this.radius - 1;
        //
        // // check for collisions on prospective movement corners
        // return !(this.map.isPassable(left, top) && this.map.isPassable(right, top) &&
        //     this.map.isPassable(right, bottom) && this.map.isPassable(left, bottom));
    }
}

export class MoveablePart {
    baseSpeed: number = 100;

    initMoveablePart(baseSpeed: number) {
        this.baseSpeed = baseSpeed;
    }

    moveSpeed(avatar: BaseEntity) {
        let scale = 1;
        if (hasMixin(this, DamageablePart)) {
            scale *= this.vitality / 100;
        }
        if (hasMixin(this, BodyPart)) {
            const parent = avatar.parentPart(this);
            if (parent && hasMixin(parent, DamageablePart)) {
                scale *= parent.vitality / 100;
            }
        }
        return scale * this.baseSpeed;
    }
}

