import {Entity} from "../entity/entity";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {TopDown} from "./top_down";
import {Attribute, scaleOnAtt} from "../entity/attributes";
import {EntitySkills, scaleOnSkill} from "../entity/skills";

export interface OngoingGrasp {
    grasper: GraspCapable;
    grasped: Graspable & BodyPart;
    actor: Entity;
    target: Entity;
}

const BASE_WRESTLE_DAMAGE = 10;
const BASE_RUB_AROUSAL = 10;

// @ts-ignore
export class GraspCapable extends TopDown {
    initGraspCapable() {
        this.initTopDown();
    }

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract graspStrength(avatar): number;

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract graspRange(avatar): number;

    pinchDamage(avatar) {
        return scaleOnAtt(avatar, Attribute.STR, 1, 2) * BASE_WRESTLE_DAMAGE;
    }

    rubArousal(avatar) {
        return scaleOnSkill(avatar, EntitySkills.RUBBING, 1, 2) * BASE_RUB_AROUSAL;
    }
    rubTerm(part: BodyPart) {
        return "rub";
    }
}

// can be grasped
export class Graspable {
}

