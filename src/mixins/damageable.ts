import {clamp} from "drawpoint/dist-esm";

export class DamageablePart {
    vitality: number;
    baseVitalityScaling: number = 1;
    baseHitDifficulty: number;
    damageListener: (DamageablePart) => void;

    initDamageablePart(baseVitalityScaling: number, baseHitDifficulty: number) {
        this.vitality = 100;
        this.baseVitalityScaling = baseVitalityScaling;
        this.baseHitDifficulty = baseHitDifficulty;
    }

    addDamageListener(listener) {
        this.damageListener = listener;
    }

    takeDamage(damage: number) {
        if (damage > 0) {
            damage = this.baseVitalityScaling * damage;
            this.vitality = clamp(this.vitality - damage, 0, 100);
            if (this.damageListener) {
                this.damageListener(this);
            }
        }
        return damage;
    }
}

