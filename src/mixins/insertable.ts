import {EntitySkills, scaleOnSkill} from "../entity/skills";

export type Volume = number; // mL

const BASE_INSERT_AROUSAL = 10;

export interface Liquid {
    desc: string,
    volume: Volume
}

export interface InsertSize {
    length: number;
    girth: number;
}

export class Orifice {
    depth: number;    // in cm?
    girth: number;
    stretch: number;
    elasticity: number; // affects both how much stretch orifice can take before permanently increasing size, and how fast recovery is
    lubrication: Liquid;

    initOrifice(depth: number, girth: number, elasticity: number, stretch: number = 0,
                lubrication: Liquid = {desc: "dry", volume: 0}) {
        this.depth = depth;
        this.girth = girth;
        this.elasticity = elasticity;
        this.stretch = stretch;
        this.lubrication = lubrication;
    }
}

export class InsertCapable {

    // @ts-ignore
    abstract insertSize(avatar): InsertSize;

    insertArousal(avatar) {
        return scaleOnSkill(avatar, EntitySkills.FUCKING, 1, 2) * BASE_INSERT_AROUSAL;
    }

    insertRange() {
        return 5;
    }
}
