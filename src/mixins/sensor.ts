import {isPose, Point} from "../basetypes";
import {angle, diff, norm} from "drawpoint/dist-esm";
import {Entity, UID} from "../entity/entity";
import {angleDiff} from "../util/math";

export interface SensorSlice {
    radius: number;
    /**
     * radian size of slice 0 to pi; one half of the FOV
     */
    halfArc: number;
}

export class Sensor {
    rememberedEntities: Record<UID, Point> = {};

    // @ts-ignore: "Abstract methods can only appear within an abstract class"
    abstract sensingSlices(): SensorSlice[];

    updateRememberedEntities(entities: Entity[]) {
        entities.forEach((entity) => {
            if (this.canSense(entity)) {
                this.rememberedEntities[entity.uid] = {x: entity.x, y: entity.y};
            } else {
                // implement forgetting
                // we know they are not there if we can sense where we remembered they are but they are not there
                const rememberedPos = this.rememberedEntities[entity.uid];
                if (rememberedPos && this.canSense(rememberedPos)) {
                    delete this.rememberedEntities[entity.uid];
                }
            }
        })
    }

    canSense(pt: Point) {
        if (isPose(this)) {
            const dxy = diff(this, pt);
            const distToPt = norm(dxy);
            const angleToPt = Math.abs(angleDiff(this.rot, angle({x: dxy.x, y: -dxy.y})));
            for (const slice of this.sensingSlices()) {
                if (distToPt < slice.radius && angleToPt < slice.halfArc) {
                    return true;
                }
            }
        }
        return false;
    }
}

