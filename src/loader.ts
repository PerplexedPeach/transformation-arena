export const images = {};

export function loadImage(key, src) {
    const img = new Image();

    const d = new Promise(function (resolve, reject) {
        img.onload = function () {
            images[key] = img;
            resolve(img);
        }.bind(this);

        img.onerror = function () {
            reject('Could not load image: ' + src);
        };
    }.bind(this));

    img.crossOrigin = "anonymous";
    img.src = src;
    return d;
}

export function getImage(key) {
    return (key in images) ? images[key] : null;
}
