import {Entity, UID} from "../entity/entity";
import {GameClass} from "../base";
import {getBaseColors} from "../../lib/dad/src"
import {Camera} from "./camera";
import {Point, Pose, TilePoint} from "../basetypes";
import {distPoint, roundPixel} from "../util/math";
import {ColorMap, TopDown} from "../mixins/top_down";
import {hasMixin} from "ts-mixer";
import {add} from "drawpoint/dist-esm";
import {Sensor} from "../mixins/sensor";
import {drawPie} from "../util/draw";

export enum Tile {
    BLANK,
    GRASS,
    FLOOR,
    TREE
}

export enum TerrainFeatures {
    BUSH = 5
}

export type MapTiles = Array<Array<number | Tile>>;

export class GameMap {
    readonly cols: number;
    readonly rows: number;
    readonly _width: number;
    readonly _height: number;
    readonly tsize: number; // units per tile (tile size)
    private readonly _entities: Record<UID, Entity>;
    public items: object;

    private readonly _bgCanvas: HTMLCanvasElement;
    private _cachedBg = false;

    constructor(public tiles: MapTiles) {
        this.tsize = 64;
        this.rows = this.tiles.length;
        this.cols = this.tiles[0].length;
        this._width = this.cols * this.tsize;
        this._height = this.rows * this.tsize;
        // clang-format on

        this.items = {};
        this._entities = {};

        // render entire background map to _bgCanvas
        this._bgCanvas = document.createElement("canvas");
        this._bgCanvas.width = this._width;
        this._bgCanvas.height = this._height;

    }

    wholeTilePoint(tilePt: TilePoint): TilePoint {
        return {x: Math.floor(tilePt.x), y: Math.floor(tilePt.y)};
    }

    getTile(tilePt: TilePoint): Tile {
        const {x, y} = this.wholeTilePoint(tilePt);
        return this.tiles[y][x];
    }

    getItems(tilePt: TilePoint): any {
        const {x, y} = this.wholeTilePoint(tilePt);
        return this.items[`${y},${x}`] || [];
    }

    isPassable(tilePt: TilePoint): boolean {
        const tile = this.getTile(tilePt);
        return tile !== 3 && tile !== 5;
    }

    tilesToPixels(tilePt: TilePoint): Point {
        return {x: tilePt.x * this.tsize, y: tilePt.y * this.tsize};
    }

    pixelsToTiles(pixelPt: Point): TilePoint {
        return {x: pixelPt.x / this.tsize, y: pixelPt.y / this.tsize};
    }

    entities() {
        return Object.values(this._entities);
    }

    entityWithUID(uid: UID) {
        return this._entities[uid];
    }

    entitiesAt(pt: Point): Entity[] {
        // multiple entities could occupy the same location
        const entities: Entity[] = [];
        this.entities().forEach((entity) => {
            if (distPoint(pt, entity) <= entity.radius) {
                entities.push(entity);
            }
        });
        return entities;
    }

    entitiesWithinRadius(pt: Point, r: number): Entity[] {
        const entities: Entity[] = [];
        this.entities().forEach((entity) => {
            if (distPoint(pt, entity) <= entity.radius + r) {
                entities.push(entity);
            }
        });
        return entities;
    }

    addEntity(entity: Entity) {
        if (entity.x < 0 || entity.x > this._width || entity.y < 0 || entity.y >= this._height) {
            throw new Error('Adding entity out of bounds');
        }
        entity.map = this;
        this._entities[entity.uid] = entity;
    }

    removeEntity(entity: Entity) {
        // find entity if it exists
        delete this._entities[entity.uid];
    }

    drawTile(ctx: CanvasRenderingContext2D, atlas: CanvasImageSource, tile, x, y) {
        // TODO decouple canvas units from actual pixel size (to allow zooming)
        if (tile !== 0) {
            ctx.drawImage(atlas,
                (tile - 1) * this.tsize,  // source x
                0,                       // source y
                this.tsize,               // source width
                this.tsize,               // source height
                roundPixel(x),      // target x (round to elminate subpixel)
                roundPixel(y),      // target y
                this.tsize,               // target width
                this.tsize                // target height
            );
        }
    }

    drawBackground(game: GameClass, camera: Camera) {
        // pre-render background on separate canvas
        if (!this._cachedBg) {
            const ctx = this._bgCanvas.getContext("2d");
            if (!ctx) {
                throw new Error("need 2d canvas context");
            }
            for (let c = 0; c < this.cols; ++c) {
                for (let r = 0; r < this.rows; ++r) {
                    const tilePt = {x: c, y: r};
                    const tile = this.getTile(tilePt);
                    const cameraPt = this.tilesToPixels(tilePt);
                    this.drawTile(ctx, game.tileAtlas, tile, cameraPt.x, cameraPt.y);
                }
            }
            this._cachedBg = true;
        }

        game.ctxs.bg.drawImage(this._bgCanvas,
            camera.x,  // source x
            camera.y,                       // source y
            game.canvasWidth,               // source width
            game.canvasHeight,               // source height
            0,
            0,
            game.canvasWidth,               // target width
            game.canvasHeight                // target height
        );
    }

    drawEntityFOV(game: GameClass, camera: Camera, sensor: Sensor, clear = false) {
        const ctx = game.ctxs.fg;
        if (clear) {
            ctx.clearRect(0, 0, game.canvasWidth, game.canvasHeight);
        }

        ctx.fillStyle = "hsla(0,0%,100%,0.1)";
        const pose = sensor as unknown as Pose;
        const offset = add(pose, camera, -1);
        sensor.sensingSlices().forEach((shadingSlice) => {
            ctx.beginPath();
            drawPie(ctx, offset, shadingSlice.radius, pose.rot - shadingSlice.halfArc, pose.rot + shadingSlice.halfArc);
            ctx.fill();
        });
    }

    drawGrid(game: GameClass, camera: Camera) {
        const width = this.cols * this.tsize;
        const height = this.rows * this.tsize;
        for (let r = 0; r < this.rows; r++) {
            const x = -camera.x;
            const y = r * this.tsize - camera.y;
            game.ctxs.bg.beginPath();
            game.ctxs.bg.moveTo(x, y);
            game.ctxs.bg.lineTo(width, y);
            game.ctxs.bg.stroke();
        }
        for (let c = 0; c < this.cols; c++) {
            const x = c * this.tsize - camera.x;
            const y = -camera.y;
            game.ctxs.bg.beginPath();
            game.ctxs.bg.moveTo(x, y);
            game.ctxs.bg.lineTo(x, height);
            game.ctxs.bg.stroke();
        }
    }

    drawEntities(game: GameClass, camera: Camera) {
        const ctx = game.ctxs.entity;
        ctx.clearRect(0, 0, game.canvasWidth, game.canvasHeight);
        this.entities().forEach((entity) => {
            ctx.lineWidth = 2;

            const pos = camera.worldToScreen(entity);

            // TODO assume all actors are Player objects for now
            const colors = getBaseColors(entity) as unknown as ColorMap;

            // TODO use more specific part for describing parts if DAD switches to TS
            const partsToRender: TopDown[] = [];
            // order parts
            entity.parts.forEach((part) => {
                if (hasMixin(part, TopDown)) {
                    partsToRender.push(part);
                }
            });
            partsToRender.sort((partA, partB) => {
                // @ts-ignore
                if (partA.topDownLayer() === partB.topDownLayer()) {
                    return 0;
                }
                // @ts-ignore
                return (partA.topDownLayer() < partB.topDownLayer()) ? 1 : -1;
            });
            partsToRender.forEach((part) => {
                part.renderTopDown(entity, ctx, pos, colors);
            });
        });
    }
}

