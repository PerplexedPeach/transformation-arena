import {clamp} from "drawpoint/dist-esm";
import {Point, ScreenPoint} from "../basetypes";
import {GameMap} from "./map";

const SPEED = 256;  // pixels per second

export class Camera implements Point {
    public x : number;
    public y : number;
    public maxX: number;
    public maxY: number;
    public following : Point | null;

    constructor(map : GameMap, readonly width, readonly height) {
        this.x = 0;
        this.y = 0;
        this.maxX = map.cols * map.tsize - width;
        this.maxY = map.rows * map.tsize - height;
        this.following = null;
    }

    // all units are in pixels
    screenToWorld({x, y}: ScreenPoint) : Point {
        return {
            x: x + this.x,
            y: y + this.y,
        };
    }

    worldToScreen({x,y}: Point) : ScreenPoint {
        return {
            x: x - this.x,
            y: y - this.y,
        };
    }

    /**
     * Move to x,y world coordinates and return if any move was made
     */
    moveTo(x, y) : boolean {
        const old = [this.x, this.y];
        this.x = clamp(x, 0, this.maxX);
        this.y = clamp(y, 0, this.maxY);
        return this.x !== old[0] || this.y !== old[1];
    }

    move(delta : number, dirx, diry) : boolean {
        // move camera
        return this.moveTo(this.x + dirx * SPEED * delta, this.y + diry * SPEED * delta);
    }

    follow(entity : Point) : void {
        this.following = entity;
    }

    unfollow() : void {
        this.following = null;
    }

    update() : boolean {
        // use camera.move if the camera is free (not attached)
        if (!this.following) {
            return false;
        }

        // make the camera follow the sprite
        return this.moveTo(this.following.x - this.width / 2, this.following.y - this.height / 2);

        // in map corners, the sprite cannot be placed in the center of the screen
        // and we have to change its screen coordinates
    }
}
