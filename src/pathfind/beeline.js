import {Pathfind} from "./pathfind";

Pathfind.beeline = {
    route(ex, ey) { return [[ex, ey]]; },
};