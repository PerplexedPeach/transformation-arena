export const Pathfind = {
    /**
     * route a path (list of waypoints (2-tuple) to be travelled in straight line)
     * to another point 'this' should be bound to the entity wishing to travel
     */
    route(algorithm, x, y) {
        return algorithm.route.call(this, x, y);
    }
};
