// relative to the perspective of the player
import {Entity} from "./entity/entity";
import {TextItem, TextType} from "./menu/text_item";
import {GameClass} from "./base";
import {BaseEntity} from "./entity/player";

export interface Message {
    sender: Entity;
    receiver: Entity;
    type: TextType; // from the perspective of the sender
    text: string;
}

interface MessageElement {
    elem: HTMLElement;
    addTime: number;
}

const MESSAGE_SHOW_DURATION = 4;
let messageTime = -1;

let archive: Message[] = [];
let archiveMaxSize = 5000;

let unprocessedMessages: Message[] = [];
const shownMessageElements: MessageElement[] = [];

let logContainer: HTMLElement;

const switchPerspective: Record<TextType, TextType> = {
    [TextType.NORMAL]     : TextType.NORMAL,
    [TextType.IMPORTANT]  : TextType.IMPORTANT,
    [TextType.UNIMPORTANT]: TextType.UNIMPORTANT,
    [TextType.GOOD]       : TextType.BAD,
    [TextType.GOOD2]      : TextType.BAD,
    [TextType.GOOD3]      : TextType.BAD2,
    [TextType.GOOD4]      : TextType.BAD3,
    [TextType.BAD]        : TextType.GOOD,
    [TextType.BAD2]       : TextType.GOOD3,
    [TextType.BAD3]       : TextType.GOOD4,
    [TextType.LUST]       : TextType.LUST,
};

export function processMessages(game: GameClass, you: BaseEntity, delta: number, usePronouns = false) {
    if (!logContainer) {
        const parent = document.getElementById("log-dom");
        const elem = document.getElementById("logs");
        if (!parent || !elem) {
            throw new Error(`Can't find DOM for logging`);
        }
        parent.style.width = `${game.canvasWidth}px`;
        logContainer = elem;
    }
    if (messageTime < -1) {
        messageTime = 0;
    } else {
        messageTime += delta;
    }

    // check for messages to remove
    while (shownMessageElements.length && shownMessageElements[0].addTime + MESSAGE_SHOW_DURATION < messageTime) {
        logContainer.removeChild(shownMessageElements[0].elem);
        shownMessageElements.shift();
    }

    // check for messages to add
    // only show messages involving an entity you can sense
    const relevantMessages = unprocessedMessages.filter(
        msg => msg.sender === you || msg.receiver === you || you.canSense(msg.sender) || you.canSense(msg.receiver));

    if (!relevantMessages.length) {
        return;
    }

    for (let msg of relevantMessages) {
        const div = document.createElement('div');
        div.classList.add('log');

        // handle reverse perspective of type if you are the target
        const type = (msg.receiver === you) ? switchPerspective[msg.type] : msg.type;
        // replace names with pronouns
        let text = msg.text;
        if (usePronouns) {
            if (msg.sender === you) {
                text = text.replace(you.name, "you");
            } else if (msg.receiver === you) {
                text = text.replace(`${you.name}'s`, "your");
            }
        }
        // adjust color by lowering saturation for allies and other entities
        const item = new TextItem(text, {type: type, desaturate: msg.sender !== you && msg.receiver !== you});

        item.addContent(div);
        logContainer.appendChild(div);
        shownMessageElements.push({elem: div, addTime: messageTime});
    }

    logContainer.scrollTop = logContainer.scrollHeight;
    const archiveStartIndex = Math.max(0, archive.length + unprocessedMessages.length - archiveMaxSize);
    archive = archive.concat(archive.slice(archiveStartIndex), unprocessedMessages);
    unprocessedMessages = [];
}

export function logMsg(msg: Message) {
    unprocessedMessages.push(msg);
}
