import {Behavior} from "./behavior";
import {Actor} from "../mixins/actor";
import {Moveable} from "../mixins/moveable";
import {add, angle, diff, norm} from "drawpoint/dist-esm";
import {changeMove} from "../action/move";
import {hasMixin} from "ts-mixer";

type FollowCondition = (actor: Actor, followData: Follower) => boolean;

export class Follower implements Behavior {
    constructor(readonly target: Moveable, private readonly minFollowDist: number,
                private readonly followCondition?: FollowCondition, private readonly followFaceAngle: number = 0) {
    }

    modifyActions(actor: Actor) {
        // can't move so do nothing
        if (!hasMixin(actor, Moveable)) {
            return false;
        }
        // by not giving a condition we are unconditionally following
        if (!this.followCondition || this.followCondition(actor, this)) {
            // add move actions straight line to target if our distance is outside the follow dist
            const dxy = diff(this.target, actor);
            // face the target
            actor.rot = Math.PI + this.followFaceAngle + angle(dxy);
            const distTo = norm(dxy);
            if (distTo > this.minFollowDist) {
                const closestPointAtDist = add(this.target, dxy, this.minFollowDist / distTo);
                changeMove(actor, closestPointAtDist);
                return true;
            }
        }
        return false;
    }
}
