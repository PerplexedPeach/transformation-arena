import {Actor} from "../mixins/actor";

export interface Behavior {
    modifyActions(actor: Actor, delta: number): boolean;
}
