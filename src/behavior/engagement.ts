import {Behavior} from "./behavior";
import {Actor} from "../mixins/actor";
import {add, angle, diff, getUnitVector, norm, origin, rotatePoints} from "drawpoint/dist-esm";
import {changeMove} from "../action/move";
import {hasMixin} from "ts-mixer";
import {Sensor} from "../mixins/sensor";
import {Entity, isEntity, Opinion, OpinionValue, UID} from "../entity/entity";
import {distPoint} from "../util/math";
import {Moveable} from "../mixins/moveable";
import {Point} from "../basetypes";
import {StrikeCapable} from "../mixins/strikeable";
import {Strike} from "../action/strike";
import {randomChoice} from "../util/util";

const RETARGET_DELAY = 1;

export interface Personality {
    confidence: OpinionValue; // against awe
    composure: OpinionValue; // against hate
    modesty: OpinionValue; // against lust
}

enum EngagementApproach {
    IGNORE,
    VIOLENCE,
    LEWDNESS,
    RUN_AWAY,
    HELPFUL
}

function engagementCondition(opinion: Opinion, personality: Personality) {
    const terror = opinion.awe - personality.confidence; // high awe and low confidence
    const aggression = opinion.hate - personality.composure; // high hate and low composure
    const infatuation = opinion.lust - personality.modesty; // high lust and low modesty
    // low aggression
    if (aggression <= -5) {
        // overwhelming infatuation
        if (infatuation > terror && infatuation >= 8) {
            return EngagementApproach.LEWDNESS;
        }
        // need to love regardless of composure to be helpful
        if (opinion.hate <= -6) {
            return EngagementApproach.HELPFUL;
        }
        return EngagementApproach.IGNORE;
    }
    if (aggression >= 4) {
        if (infatuation > aggression) {
            return EngagementApproach.LEWDNESS;
        }
        if (terror > aggression) {
            return EngagementApproach.RUN_AWAY;
        }
        return EngagementApproach.VIOLENCE;
    }
    // aggression is somewhere in the middle
    if (infatuation > 5 && infatuation > terror) {
        // scaroused
        return EngagementApproach.LEWDNESS;
    }
    if (terror > 7) {
        // terrified
        return EngagementApproach.RUN_AWAY;
    }
    return EngagementApproach.IGNORE;
}

export class Engagement implements Behavior {
    private _lastRetarget = 0;
    private _targetUID: UID | null;

    constructor(private readonly _maxConsiderDist: number, readonly personality: Personality) {
    }

    modifyActions(actor: Actor, delta: number) {
        // can't sense any entities
        if (!hasMixin(actor, Sensor) || !isEntity(actor)) {
            return false;
        }

        // find a target (and keep that target for some time)
        this._lastRetarget += delta;
        if (this._lastRetarget >= RETARGET_DELAY) {
            this._targetUID = this.findClosestTarget(actor);
            this._lastRetarget = 0;
        }

        // decide what actions to do with this target depending on our opinion of them
        if (!this._targetUID) {
            return false;
        }

        const targetPt = actor.rememberedEntities[this._targetUID];
        if (!targetPt) {
            return false;
        }

        const entity = actor.map.entityWithUID(this._targetUID);
        // const d = distPoint(entity, actor);

        switch (engagementCondition(actor.opinion(entity), this.personality)) {
            case EngagementApproach.HELPFUL:
                // TODO implement helpful (maybe follow?)
                break;
            case EngagementApproach.LEWDNESS:
                // TODO get close enough to do a lewd attack/engage in grapple
                break;
            case EngagementApproach.VIOLENCE:
                return this.doViolence(actor, targetPt, entity);
            case EngagementApproach.RUN_AWAY:
                if (hasMixin(actor, Moveable)) {
                    return this.runAway(actor, targetPt);
                }
                return false;
            case EngagementApproach.IGNORE:
                return false;
        }
        return false;
    }

    doViolence(actor: Actor, pt: Point, entity: Entity) {
        if (!isEntity(actor)) {
            return false;
        }
        const currentAttack = actor.getUpcomingActionOfType(Strike);
        // continue current attack
        if (currentAttack) {
            return true;
        }

        const dxy = diff(pt, actor);
        const distTo = norm(dxy);

        // TODO decide how to attack
        let attackPart: StrikeCapable | null = null;
        const candidates: StrikeCapable[] = [];
        for (let part of actor.parts) {
            if (hasMixin(part, StrikeCapable)) {
                candidates.push(part);
            }
        }
        if (candidates.length) {
            attackPart = randomChoice(candidates);
        }

        // no parts to attack with
        if (!attackPart) {
            return false;
        }

        const actionRange = attackPart.strikeRange(actor) + entity.radius - 10;

        // too far to apply the action
        if (distTo > actionRange) {
            // can't move
            if (!hasMixin(actor, Moveable)) {
                return false;
            }

            // move to apply
            actor.rot = Math.PI + angle(dxy);
            const closestPointAtDist = add(pt, dxy, actionRange / distTo);
            changeMove(actor, closestPointAtDist);
            return true;
        } else {
            const dir = getUnitVector(add(dxy, attackPart.offset(actor)));
            // apply the attack action
            if (hasMixin(actor, Moveable)) {
                actor.rot = Math.PI + angle(dxy);
                rotatePoints(origin, Math.PI - actor.rot, dir);
            }
            actor.queueAction(new Strike(actor, attackPart, dir));
            return true;
        }
    }

    runAway(actor: Actor & Moveable, pt: Point) {
        const dxy = diff(pt, actor);
        // face the target
        actor.rot = Math.PI + angle(dxy);
        const distTo = norm(dxy);
        if (distTo < this._maxConsiderDist) {
            const closestPointAtDist = add(pt, dxy, this._maxConsiderDist / distTo);
            changeMove(actor, closestPointAtDist);
            return true;
        }
        return false;
    }

    findClosestTarget(actor: Actor & Sensor & Entity): UID | null {
        let closestWithEngagement: UID | null = null;
        let closestDist = this._maxConsiderDist;
        for (let [uid, pt] of Object.entries(actor.rememberedEntities)) {
            // ignore self
            if (uid === actor.uid) {
                continue;
            }
            const entity = actor.map.entityWithUID(uid);
            const d = distPoint(pt, actor);
            if (d < this._maxConsiderDist && d < closestDist && engagementCondition(actor.opinion(entity),
                this.personality) !== EngagementApproach.IGNORE) {
                closestDist = d;
                closestWithEngagement = uid;
            }
        }
        return closestWithEngagement;
    }
}
