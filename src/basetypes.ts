export interface Point {
    x: number;
    y: number;
}

// differentiate it from world Point
export interface ScreenPoint extends Point {
}

export interface TilePoint extends Point {
}

export interface Pose extends Point {
    rot: number;
}

export function isPose(obj: any): obj is Pose {
    return 'rot' in obj;
}