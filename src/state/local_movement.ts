import {State} from "./state";
import {Keys} from "../input/keyboard";
import {getMovement} from "../util/util";
import {changeMove, Move} from "../action/move";
import {getClickScreenPosition, Mouse} from "../input/mouse";
import {add, diff} from "drawpoint/dist-esm";
import {GameClass} from "../base";
import {Strike} from "../action/strike";
import {ContextMenu, ContextMenuItem} from "./context_menu";
import {Point, ScreenPoint} from "../basetypes";
import {GameMap} from "../map/map";
import {Camera} from "../map/camera";
import {StrikeCapable} from "../mixins/strikeable";
import {Actor} from "../mixins/actor";
import {BaseEntity} from "../entity/player";
import {hasMixin} from "ts-mixer";
import {Sensor} from "../mixins/sensor";
import {InventoryState} from "./inventory";
import {ExamineState} from "./examine";
import {TargetMenu} from "./target_menu";
import {TextItem, TextType} from "../menu/text_item";
import {distPoint} from "../util/math";
import {GraspCapable} from "../mixins/graspable";
import {WrestleMenu} from "./wrestle_menu";
import {StopWrestle} from "../action/wrestle";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {logMsg, processMessages} from "../log";
import {LickCapable} from "../mixins/lickable";
import {InsertCapable} from "../mixins/insertable";

const MIN_MOVE_MAG = 10;

// TODO aim screen separate from target
enum StateTransitions {
    TOGGLE_FOLLOW,
    REFRESH_AVATAR,
    INVENTORY,
    TARGET_ATTACK,
    WRESTLE
}

// TODO allow keys to be changed
const keymap: Record<StateTransitions, string> = {
    [StateTransitions.TOGGLE_FOLLOW] : 'x',
    [StateTransitions.REFRESH_AVATAR]: 'q',
    [StateTransitions.INVENTORY]     : 'i',
    [StateTransitions.TARGET_ATTACK] : 't',
    [StateTransitions.WRESTLE]       : 'r',
};

const displayedActions: Partial<Record<StateTransitions, string>> = {
    [StateTransitions.INVENTORY]    : 'inventory',
    [StateTransitions.TARGET_ATTACK]: 'target',
    [StateTransitions.WRESTLE]      : 'wrestle',
};

class LocalMovementClass extends State {
    map: GameMap;
    camera: Camera;
    scrolled: boolean;

    actorsMoved: boolean = true;
    pointerMoved: boolean = true;

    player: BaseEntity;
    pointer: Point;
    moveDir: Point | null;

    constructor() {
        super();
    }

    init(game: GameClass, player: BaseEntity, map: GameMap) {
        this.map = map;
        this.player = player;
        this.handleResize(game);
        this.pointer = {x: 0, y: 0};
    }

    entry(game: GameClass) {
        this.scrolled = true;
        this.camera.follow(this.player);
        // initially "scrolled" to force background draw
        this.camera.update();
    }

    handleResize(game: GameClass) {
        super.handleResize(game);
        this.camera = new Camera(this.map, game.canvasWidth, game.canvasHeight);
        this.camera.follow(this.player);
        this.camera.update();
        this.player.addMoveListener((moved) => {
            if (moved && this.camera.update()) {
                this.scrolled = true;
            }
        });
    }

    handleKeyDown(game: GameClass, keys) {
        // examine around
        if (keys[keymap[StateTransitions.TOGGLE_FOLLOW]]) {
            this._toggleFollow();
            return;
        }
        if (keys[keymap[StateTransitions.REFRESH_AVATAR]]) {
            game.focusNeedsRedraw = true;
            return;
        }
        if (keys[keymap[StateTransitions.INVENTORY]]) {
            InventoryState.entry(game, this.player);
            return;
        }
        if (keys[keymap[StateTransitions.TARGET_ATTACK]]) {
            // only enter if there is some entity that can be attacked
            // TODO implement weapons
            const strikeParts = this.player.partsWithMixin(StrikeCapable);
            if (!strikeParts.length) {
                return;
            }
            const maxRange = Math.max(...strikeParts.map(part => part.strikeRange(this.player)));
            const targetsInRange = this.map.entitiesWithinRadius(this.player, maxRange)
                .filter((entity) => this.player !== entity && entity.damageable());

            if (targetsInRange.length) {
                TargetMenu.entry(game, this.player, strikeParts, targetsInRange);
            }
            return;
        }
        if (keys[keymap[StateTransitions.WRESTLE]]) {
            const graspParts = this.player.partsWithMixin(GraspCapable);
            if (!graspParts.length) {
                return;
            }
            const maxRange = Math.max(...graspParts.map(part => part.graspRange(this.player)));
            const targetsInRange = this.map.entitiesWithinRadius(this.player, maxRange + this.player.radius)
                .filter((entity) => this.player !== entity && entity.graspable());
            if (targetsInRange.length) {
                // add other parts
                const lickParts = this.player.partsWithMixin(LickCapable);
                const insertParts = this.player.partsWithMixin(InsertCapable);
                WrestleMenu.entry(game, this.player, [...graspParts, ...lickParts, ...insertParts], targetsInRange);
            }
            return;
        }
        this.moveDir = getMovement(keys);
        // TODO come up with a better way of unpausing on player action
        // unpause if the keys we pressed will produce an action
        if (this._scheduleMovePlayer()) {
            game.paused = false;
        }
    }

    handleKeyUp(game: GameClass, keys: Keys) {
        this.moveDir = getMovement(keys);
    }

    _scheduleMovePlayer() {
        if (this.moveDir && this.camera.following === this.player) {
            // when grasping, treat movement as trying to break grapple
            if (this.player.wrestling.length) {
                // if we're already struggling to get out, just wait for that to finish
                const existingStruggle = this.player.getUpcomingActionOfType(StopWrestle);
                if (!existingStruggle) {
                    console.log('escaping grasp');
                    const grasp = this.player.wrestling[0];
                    const target = (grasp.target === this.player) ? grasp.actor : grasp.target;
                    this.player.queueAction(new StopWrestle(this.player, target))
                }
            } else {
                changeMove(this.player, add(this.player, this.moveDir, MIN_MOVE_MAG));
            }
            return true;
        }
        return false;
    }


    handleMouseUp(game: GameClass, e) {
        if (e.button === Mouse.LEFT) {
            // TODO add default config of left clicking
            const side = (Math.random() > 0.5) ? "left " : "right ";
            // const dir = getUnitVector(diff(Data.player, Data.pointer));
            const part = this.player.getPartInLocation(side + "hand") as BodyPart;
            if (hasMixin(part, StrikeCapable) && !this.player.partIsWrestling(part)) {
                // rotatePoints({x:0,y:0}, Data.player.rot, dir);
                const action = new Strike(this.player, part, {
                    x: 1,
                    y: 0
                });
                this.player.queueAction(action);
                game.paused = false;
            }
        }
    }

    handleMouseMove(game: GameClass, e) {
        // TODO allow rotation at slower speed when wrestling rather than not at all
        if (this.player.wrestling.length === 0) {
            const screenXY = getClickScreenPosition(game, e);
            this.pointer = this.camera.screenToWorld(screenXY);
            this.pointerMoved = true;
            const toPointer = diff(this.player, this.pointer);
            this.player.rot = Math.atan2(toPointer.y, toPointer.x);
        }
    }

    updateChanges(game: GameClass, delta) {
        this._scheduleMovePlayer();
        processMessages(game, this.player, delta);

        let playerActionTaken = false;
        // process actions for actors
        this.map.entities().forEach((entity) => {
            if (hasMixin(entity, Actor)) {
                const [tookAction, finishedAction] = entity.takeAction(delta);
                if (tookAction) {
                    if (entity === this.player) {
                        playerActionTaken = true;
                    }
                    this.actorsMoved = true;
                }
            }
            if (hasMixin(entity, Sensor)) {
                entity.updateRememberedEntities(this.map.entities());
            }
        });

        // pause after finishing updates, waiting for next action
        if (!playerActionTaken && game.pauseWhenNotTakingAction) {
            game.paused = true;
        }
    }

    updateReadOnly(game: GameClass, delta: number) {
        // just changing the camera without changing state
        if (this.camera.following === null) {
            if (this.moveDir) {
                if (this.camera.move(delta, this.moveDir.x, this.moveDir.y)) {
                    this.scrolled = true;
                    // relative movement to the camera
                    this.actorsMoved = true;
                }
            }
        }
    }

    render(game: GameClass, force = false) {
        if (this.scrolled || force) {
            this.map.drawBackground(game, this.camera);
            if (game.debug) {
                this.map.drawGrid(game, this.camera);
            }
            this.scrolled = false;
        }
        if (this.actorsMoved || this.pointerMoved || force) {
            this.map.drawEntities(game, this.camera);
            this.drawStatusBar(game);
            if (game.drawPlayerFOV) {
                this.map.entities().forEach((entity, i) => {
                    if (hasMixin(entity, Sensor)) {
                        this.map.drawEntityFOV(game, this.camera, entity, i === 0);
                    }
                })
            }
            this.actorsMoved = false;
            this.pointerMoved = false;
        }
    }

    drawStatusBar(game: GameClass) {
        game.statusBar.innerHTML = '';

        for (let [transition, name] of Object.entries(displayedActions)) {
            if (!name) {
                continue;
            }
            const shortcutKey = keymap[transition];
            const shortcut = `${shortcutKey} - `;
            const item = new TextItem(shortcut, {type: TextType.GOOD2});
            item.chain(new TextItem(name, {type: TextType.IMPORTANT}))
                .chain(new TextItem('   '))
                .addContent(game.statusBar);
        }

        const item = new TextItem("move speed ");
        item.chain(new TextItem(Math.round(this.player.moveSpeed).toString(), {type: TextType.GOOD2}));
        item.addContent(game.statusBar);
    }


    handleContextMenu(game: GameClass, screenPoint: ScreenPoint) {
        const pixelPt = this.camera.screenToWorld(screenPoint);
        const tilePt = this.map.pixelsToTiles(pixelPt);
        // list of actions, each of which should be convertible to string

        const actions: ContextMenuItem[] = [];
        const items = this.map.getItems(tilePt);
        // take actions against the items here
        for (let i = 0; i < items.length; ++i) {
            actions.concat(items[i].getActionsAvailable(this.player));
        }

        // move to this point
        if (this.map.isPassable(tilePt)) {
            actions.push(new Move(this.player, pixelPt));
        }

        const entities = this.map.entitiesAt(pixelPt);
        entities.forEach((entity) => {
            actions.push({
                toString: () => `examine ${entity.name || entity.uid}`, execute(): void {
                    ExamineState.entry(game, entity);
                }
            });

            if (entity === this.player) {
                return;
            }

            const strikeParts = this.player.partsWithMixin(StrikeCapable);
            if (!strikeParts.length || !entity.damageable()) {
                return;
            }
            const maxRange = Math.max(...strikeParts.map(part => part.strikeRange(this.player)));
            if (distPoint(this.player, entity) < maxRange + entity.radius) {
                actions.push({
                    toString: () => `target ${entity.name || entity.uid}`, execute: () => {
                        TargetMenu.entry(game, this.player, strikeParts, [entity]);
                    }
                });
            }
        });

        ContextMenu.entry(game, this.player, screenPoint, actions);
    }

    _toggleFollow() {
        if (this.camera.following) {
            this.camera.unfollow();
        } else {
            this.camera.follow(this.player);
        }
        this.camera.update();
        this.scrolled = true;
        this.actorsMoved = true;
    }
}

export const LocalMovement = new LocalMovementClass();

