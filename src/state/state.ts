import {GameClass} from "../base";
import {Keys} from "../input/keyboard";
import {ScreenPoint} from "../basetypes";

export abstract class State {
    public requireRerender: boolean;

    protected constructor() {
        this.requireRerender = true;
    }

    init(game: GameClass, ...args: any[]) {
    }

    abstract entry(game: GameClass, ...args: any[]): void;

    handleKeyDown(game: GameClass, keys: Keys) {
    }

    handleKeyUp(game: GameClass, keys: Keys) {
    }

    handleMouseMove(game: GameClass, e) {
    }

    handleMouseUp(game: GameClass, e) {
    }

    handleContextMenu(game: GameClass, screenPoint: ScreenPoint) {
    }

    handleResize(game: GameClass) {
        this.requireRerender = true;
    }

    // done every game update (every frame)
    updateChanges(game: GameClass, delta: number) {
    }

    updateReadOnly(game: GameClass, delta: number) {
    }

    abstract render(game: GameClass, force: boolean): void;
}
