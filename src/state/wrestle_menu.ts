import {GameClass} from "../base";
import {BaseEntity} from "../entity/player";
import {MenuItem, MenuState} from "../menu/menu";
import {HeadingItem, SelectConfig, SelectionItem, TextItem, TextType} from "../menu/text_item";
import {Entity} from "../entity/entity";
import {distPoint} from "../util/math";
import {hasMixin} from "ts-mixer";
import {Actor} from "../mixins/actor";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {add, diff, origin, rotatePoints} from "drawpoint/dist-esm";
import {Graspable, GraspCapable} from "../mixins/graspable";
import {Wrestle} from "../action/wrestle";
import {Arousable} from "../mixins/arousable";
import {Insert, Lick, Rub} from "../action/sexual";
import {LickCapable} from "../mixins/lickable";
import {InsertCapable, Orifice} from "../mixins/insertable";

const textConfig = {padding: {x: 60, y: 2}};

export type WrestleCapable = GraspCapable | LickCapable | InsertCapable;
export type WrestleReceiver = Graspable | Arousable;

export class WrestleMenuClass extends MenuState {
    player: BaseEntity;
    wrestleParts: WrestleCapable[];
    targets: Entity[];

    constructor() {
        super();
    }

    _rootMenuList(): MenuItem[] {
        // TODO factor out common components with target menu
        if (this.targets.length === 1) {
            return this.targetProducer(this.targets[0]);
        }
        const menu: MenuItem[] = [];
        menu.push(new HeadingItem("Wrestling"));
        this.targets.forEach((entity) => {
            let text = `${entity.name} (${Math.round(distPoint(this.player, entity) - entity.radius)} away)`;
            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.targetProducer.bind(this, entity));
            };
            menu.push(new SelectionItem(doEnter, text, {type: TextType.NORMAL}));
            if (hasMixin(entity, Actor)) {
                // TODO include their intention towards you (hostile, ...)

            }
        });
        return menu;
    }

    partWorldPos(part) {
        const offset = (part.offset) ? part.offset(this.player) : {x: 0, y: 0};
        return add(this.player, offset);
    }

    targetProducer(entity: Entity) {
        if (this.wrestleParts.length === 1) {
            return this.partTargetProducer(entity, this.wrestleParts[0]);
        }

        const newMenu: MenuItem[] = [];
        newMenu.push(new HeadingItem(`Wrestling ${entity.name}`));

        for (let part of this.wrestleParts) {
            // consider distance from where the part starts off (add offset)
            let partRange = 0;
            if (hasMixin(part, GraspCapable)) {
                partRange = part.graspRange(this.player);
            } else if (hasMixin(part, LickCapable)) {
                partRange = part.lickRange();
            } else if (hasMixin(part, InsertCapable)) {
                partRange = part.insertRange();
            }
            const cfg: Partial<SelectConfig> = {};
            let text;

            if (hasMixin(part, BodyPart)) {
                text = `wrestle with ${part.toString()}`;
            }

            const distToTarget = distPoint(this.partWorldPos(part), entity) - entity.radius;
            if (distToTarget < partRange) {
                text += ` (${Math.max(0, Math.round(distToTarget))} away)`;
                cfg.selectable = true;
            } else {
                text += ` (${Math.round(partRange)} range)`;
                cfg.selectable = false;
                cfg.type = TextType.UNIMPORTANT;
            }

            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.partTargetProducer.bind(this, entity, part));
            };
            const selectionItem = new SelectionItem(doEnter, text, {...textConfig, ...cfg});

            // TODO list other wrestle methods here
            if (hasMixin(part, GraspCapable)) {
                // TODO list grasp strength
                selectionItem.chain(new TextItem("(grasp)", {type: TextType.GOOD4}));
            }
            if (hasMixin(part, LickCapable)) {
                selectionItem.chain(new TextItem("(lick)", {type: TextType.LUST}));
            }
            if (hasMixin(part, InsertCapable)) {
                selectionItem.chain(new TextItem("(insert)", {type: TextType.LUST}));
            }

            // check if ongoing grasp with this part
            const ongoingGrasp = this.player.wrestling.find(
                grasp => grasp.actor === this.player && grasp.grasper === part);
            if (ongoingGrasp) {
                selectionItem.chain(
                    new TextItem(` grasping ${ongoingGrasp.target.name}'s ${ongoingGrasp.grasped.toString()}`,
                        {type: TextType.GOOD3}));
            }
            // TODO check for ongoing insertion (treat similar to grasp?)

            newMenu.push(selectionItem);
        }
        return newMenu;
    }

    partTargetProducer(entity: Entity, attackPart: WrestleCapable) {
        const newMenu: MenuItem[] = [];

        let heading = `Wrestle ${entity.name}'s part with `;
        if (hasMixin(attackPart, BodyPart)) {
            heading += attackPart.toString();
        }
        newMenu.push(new HeadingItem(heading));

        // can only grasp one thing at a time
        const ongoingGrasp = this.player.wrestling.find(
            grasp => grasp.actor === this.player && grasp.target === entity && grasp.grasper === attackPart);
        if (ongoingGrasp) {
            // directly go into action selection
            return this.optionsProducer(entity, attackPart, ongoingGrasp.grasped);
        }

        // starting a new grasp, choose which part to grasp
        for (let targetPart of entity.parts) {
            const canDoAction = (hasMixin(attackPart, GraspCapable) && hasMixin(targetPart, Graspable)) ||
                (hasMixin(attackPart, LickCapable) && hasMixin(targetPart, Arousable)) ||
                (hasMixin(attackPart, InsertCapable) && hasMixin(targetPart, Orifice));
            if (!canDoAction) {
                continue;
            }

            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.optionsProducer.bind(this, entity, attackPart, targetPart));
            };
            // TODO if there's only 1 option to start wrestling with the part (and can't do sexual wrestling so don't need menu, then just do it)
            const selectionItem = new SelectionItem(doEnter, targetPart.toString()!, textConfig);

            newMenu.push(selectionItem);
        }
        return newMenu;
    }

    optionsProducer(entity: Entity, attackPart: WrestleCapable,
                    targetPart: (Graspable | Arousable | Orifice) & BodyPart) {
        const newMenu: MenuItem[] = [];
        newMenu.push(
            new HeadingItem(`Wrestling ${entity.name}'s ${targetPart.toString()} with ${attackPart.toString()}`));

        const ongoingGrasp = this.player.wrestling.find(
            grasp => grasp.actor === this.player && grasp.target === entity && grasp.grasper === attackPart);

        const dxy = diff(this.partWorldPos(attackPart), entity);
        rotatePoints(origin, -this.player.rot, dxy);

        if (hasMixin(attackPart, GraspCapable)) {
            if (ongoingGrasp) {
                const selectionItem = new SelectionItem((game, menu) => {
                    this.player.queueAction(new Wrestle(this.player, attackPart, {x: 0, y: 0}, entity, targetPart));
                    game.paused = false;
                    menu.clearMenu(game);
                }, `pinch`, {...textConfig, type: TextType.GOOD3});
                newMenu.push(selectionItem);
            } else {
                if (hasMixin(targetPart, Graspable)) {
                    const selectionItem = new SelectionItem((game, menu) => {
                        this.player.queueAction(new Wrestle(this.player, attackPart, dxy, entity, targetPart));
                        game.paused = false;
                        menu.clearMenu(game);
                    }, `grasp`, textConfig);
                    newMenu.push(selectionItem);
                }
            }
        }

        // sexual options
        // TODO handle ongoing sexual attacks
        if (hasMixin(targetPart, Arousable)) {
            if (hasMixin(attackPart, GraspCapable)) {
                const selectionItem = new SelectionItem((game, menu) => {
                    this.player.queueAction(new Rub(this.player, attackPart, dxy, entity, targetPart));
                    game.paused = false;
                    menu.clearMenu(game);
                }, attackPart.rubTerm(targetPart), {...textConfig, type: TextType.LUST});
                newMenu.push(selectionItem);
            }
            if (hasMixin(attackPart, LickCapable)) {
                const selectionItem = new SelectionItem((game, menu) => {
                    this.player.queueAction(new Lick(this.player, attackPart, entity, targetPart));
                    game.paused = false;
                    menu.clearMenu(game);
                }, "lick", {...textConfig, type: TextType.LUST});
                newMenu.push(selectionItem);
            }
            // TODO check for orifice blocking by clothing and other
            if (hasMixin(attackPart, InsertCapable) && hasMixin(targetPart, Orifice)) {
                const selectionItem = new SelectionItem((game, menu) => {
                    this.player.queueAction(new Insert(this.player, attackPart, entity, targetPart));
                    game.paused = false;
                    menu.clearMenu(game);
                }, "fuck", {...textConfig, type: TextType.LUST});
                newMenu.push(selectionItem);
            }
        }

        if (ongoingGrasp && hasMixin(attackPart, GraspCapable)) {
            // releasing own grip is instantaneous
            const releaseItem = new SelectionItem((game, menu) => {
                attackPart.displacement = {x: 0, y: 0};
                const thisGrasp = this.player.wrestling.find(
                    grasp => grasp.grasper === attackPart && grasp.grasped === targetPart);
                this.player.wrestling = this.player.wrestling.filter(grasp => grasp !== thisGrasp);
                entity.wrestling = entity.wrestling.filter(grasp => grasp !== thisGrasp);
                menu.restoreMenu(game);
            }, `release grip`, {...textConfig, type: TextType.UNIMPORTANT});
            newMenu.push(releaseItem);
        }

        return newMenu;
    }

    entry(game: GameClass, player: BaseEntity, wrestleParts: WrestleCapable[], targets: Entity[]) {
        game.enterState(this);
        game.paused = true;
        this.player = player;
        this.wrestleParts = wrestleParts;
        this.targets = targets;

        this.menuEntry(game, {
            displaySize: {x: game.canvasWidth, y: 250},
            padding    : {x: 50, y: 10},
        });
    }

    menuExit(game: GameClass) {
    }
}

export const WrestleMenu = new WrestleMenuClass();