import {GameClass} from "../base";
import {BaseEntity} from "../entity/player";
import {MenuConfig, MenuItem, MenuState} from "../menu/menu";
import {HeadingItem, SelectionItem, TextItem} from "../menu/text_item";
import {pushClothingList, pushClothingProperties} from "../menu/items";

class ExamineClass extends MenuState {
    prevFocused: BaseEntity;
    examined: BaseEntity;

    constructor() {
        super();
    }

    _rootMenuList(): MenuItem[] {
        const menu: MenuItem[] = [];
        menu.push(new HeadingItem(this.examined.name));
        menu.push(new TextItem(this.examined.desc()));

        menu.push(new HeadingItem("Properties"));
        const maxPropertyLength = 12;
        menu.push(new TextItem("base: ".padStart(maxPropertyLength) + this.examined.skeleton));
        menu.push(new TextItem("move speed: ".padStart(maxPropertyLength) + this.examined.moveSpeed));

        menu.push(new HeadingItem("Parts"));
        this.examined.parts.forEach((part) => {
            // TODO use color to indicate vitality status of part
            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.partProducer.bind(this, part));
            };
            menu.push(new SelectionItem(doEnter, `${part.toString()} (${part.constructor.name})`));
        });

        pushClothingList.call(this, menu, this.examined, this.clothesProducer);
        return menu;
    }

    partProducer(part) {
        const newMenu: MenuItem[] = [];
        newMenu.push(new HeadingItem(`${part.constructor.name}`));
        newMenu.push(new TextItem(`location: ${part.toString()} (${part.loc})`));
        return newMenu;
    }

    clothesProducer(clothes) {
        const newMenu: MenuItem[] = [];
        pushClothingProperties(newMenu, clothes);
        return newMenu;
    }

    entry(game: GameClass, examinee, menuConfig?: MenuConfig) {
        game.enterState(this);
        this.examined = examinee;
        this.prevFocused = game.focusedEntity;
        game.focusedEntity = examinee;
        game.focusNeedsRedraw = true;
        this.menuEntry(game, menuConfig);
    }

    menuExit(game: GameClass) {
        game.focusedEntity = this.prevFocused;
        game.focusNeedsRedraw = true;
    }

}

export const ExamineState = new ExamineClass();