import {GameClass} from "../base";
import {BaseEntity} from "../entity/player";
import {MenuItem, MenuState} from "../menu/menu";
import {HeadingItem, SelectConfig, SelectionItem, TextItem, TextType} from "../menu/text_item";
import {StrikeCapable} from "../mixins/strikeable";
import {Entity} from "../entity/entity";
import {distPoint} from "../util/math";
import {hasMixin} from "ts-mixer";
import {Actor} from "../mixins/actor";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {DamageablePart} from "../mixins/damageable";
import {Strike} from "../action/strike";
import {add, diff, origin, rotatePoints} from "drawpoint/dist-esm";

const textConfig = {padding: {x: 60, y: 2}};

export class TargetMenuClass extends MenuState {
    player: BaseEntity;
    strikeParts: StrikeCapable[];
    targets: Entity[];

    constructor() {
        super();
    }

    _rootMenuList(): MenuItem[] {
        if (this.targets.length === 1) {
            return this.targetProducer(this.targets[0]);
        }
        const menu: MenuItem[] = [];
        menu.push(new HeadingItem("Target attack"));
        this.targets.forEach((entity) => {
            let text = `${entity.name} (${Math.round(distPoint(this.player, entity) - entity.radius)} away)`;
            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.targetProducer.bind(this, entity));
            };
            menu.push(new SelectionItem(doEnter, text, {type: TextType.NORMAL}));
            if (hasMixin(entity, Actor)) {
                // TODO include their intention towards you (hostile, ...)

            }
        });
        return menu;
    }

    partWorldPos(part) {
        return add(this.player, part.offset(this.player));
    }

    targetProducer(entity: Entity) {
        // TODO handle weapons
        if (this.strikeParts.length === 1 && !this.player.partIsWrestling(this.strikeParts[0] as unknown as BodyPart)) {
            return this.partTargetProducer(entity, this.strikeParts[0]);
        }

        const newMenu: MenuItem[] = [];
        newMenu.push(new HeadingItem(`Attacking ${entity.name}`));

        for (let part of this.strikeParts) {
            // consider distance from where the part starts off (add offset)
            const distToTarget = distPoint(this.partWorldPos(part), entity) - entity.radius;
            const strikeRange = part.strikeRange(this.player);
            const cfg: Partial<SelectConfig> = {};
            let text;

            let actionable = distToTarget < strikeRange;
            // TODO handle weapons
            if (hasMixin(part, BodyPart)) {
                text = `${part.strikeTerm} with ${part.toString()}`;
                if (this.player.partIsWrestling(part)) {
                    actionable = false;
                    text += " (wrestling)";
                }
            }

            if (actionable) {
                // TODO handle text for weapons
                text += ` (${Math.round(distToTarget)} away)`;
                cfg.selectable = true;
            } else {
                text += ` (${Math.round(part.strikeRange(this.player))} range)`;
                cfg.selectable = false;
                cfg.type = TextType.UNIMPORTANT;
            }

            const doEnter = (game: GameClass, menu: MenuState) => {
                menu.enterMenu(this.partTargetProducer.bind(this, entity, part));
            };
            const selectionItem = new SelectionItem(doEnter, text, {...textConfig, ...cfg});

            const duration = part.strikeDuration(this.player);
            if (duration < 0.15) {
                selectionItem.chain(new TextItem(` almost instant`, {type: TextType.GOOD4}));
            } else if (duration < 0.2) {
                selectionItem.chain(new TextItem(` very quick`, {type: TextType.GOOD3}));
            } else if (duration < 0.25) {
                selectionItem.chain(new TextItem(` quick`, {type: TextType.GOOD2}));
            } else if (duration < 0.35) {
                selectionItem.chain(new TextItem(` normal`, {type: TextType.NORMAL}));
            } else if (duration < 0.5) {
                selectionItem.chain(new TextItem(` slow`, {type: TextType.BAD}));
            } else if (duration < 1) {
                selectionItem.chain(new TextItem(` very slow`, {type: TextType.BAD2}));
            } else {
                selectionItem.chain(new TextItem(` like molasses`, {type: TextType.BAD3}));
            }
            // TODO allow mouseover for detailed view
            selectionItem.chain(
                new TextItem(` ${Math.round(1000 * duration)}`, {type: TextType.UNIMPORTANT}));

            newMenu.push(selectionItem);
        }
        return newMenu;
    }

    partTargetProducer(entity: Entity, attackPart: StrikeCapable) {
        const newMenu: MenuItem[] = [];

        let heading = `${attackPart.strikeTerm}ing ${entity.name} with `;
        if (hasMixin(attackPart, BodyPart)) {
            heading += attackPart.toString();
        }
        newMenu.push(new HeadingItem(heading));

        for (let targetPart of entity.parts) {
            if (!hasMixin(targetPart, DamageablePart)) {
                continue;
            }

            const dxy = diff(this.partWorldPos(attackPart), entity);
            rotatePoints(origin, -this.player.rot, dxy);

            const selectionItem = new SelectionItem((game, menu) => {
                this.player.queueAction(
                    new Strike(this.player, attackPart, dxy, entity,
                        targetPart as unknown as DamageablePart));
                game.paused = false;
                menu.clearMenu(game);
            }, targetPart.toString()!, textConfig);

            const hitProp = attackPart.calculateHit(this.player, entity, targetPart);

            if (hitProp.probability > 0.9) {
                selectionItem.chain(new TextItem(` simple hit`, {type: TextType.GOOD3}))
            } else if (hitProp.probability > 0.7) {
                selectionItem.chain(new TextItem(` very easy`, {type: TextType.GOOD2}))
            } else if (hitProp.probability > 0.5) {
                selectionItem.chain(new TextItem(` easy`, {type: TextType.GOOD}))
            } else if (hitProp.probability > 0.35) {
                selectionItem.chain(new TextItem(` normal`, {type: TextType.NORMAL}))
            } else if (hitProp.probability > 0.22) {
                selectionItem.chain(new TextItem(` hard`, {type: TextType.BAD}))
            } else if (hitProp.probability > 0.1) {
                selectionItem.chain(new TextItem(` very hard`, {type: TextType.BAD2}))
            } else {
                selectionItem.chain(new TextItem(` impossible`, {type: TextType.BAD3}))
            }
            selectionItem
                .chain(new TextItem(` (${hitProp.probability.toFixed(2)})`, {type: TextType.UNIMPORTANT}));

            if (hitProp.damage > 50) {
                selectionItem.chain(new TextItem(` devastating`, {type: TextType.GOOD3}))
            } else if (hitProp.damage > 40) {
                selectionItem.chain(new TextItem(` very square`, {type: TextType.GOOD2}))
            } else if (hitProp.damage > 30) {
                selectionItem.chain(new TextItem(` square`, {type: TextType.GOOD}))
            } else if (hitProp.damage > 20) {
                selectionItem.chain(new TextItem(` normal`, {type: TextType.NORMAL}))
            } else if (hitProp.damage > 13) {
                selectionItem.chain(new TextItem(` can't quite land`, {type: TextType.BAD}))
            } else if (hitProp.damage > 7) {
                selectionItem.chain(new TextItem(` graze`, {type: TextType.BAD2}))
            } else {
                selectionItem.chain(new TextItem(` barely a scratch`, {type: TextType.BAD3}))
            }
            selectionItem
                .chain(new TextItem(` (${Math.round(hitProp.damage)})`, {type: TextType.UNIMPORTANT}));

            newMenu.push(selectionItem);
        }
        return newMenu;
    }

    entry(game: GameClass, player: BaseEntity, strikeParts: StrikeCapable[], targets: Entity[]) {
        game.enterState(this);
        game.paused = true;
        this.player = player;
        this.strikeParts = strikeParts;
        this.targets = targets;

        this.menuEntry(game, {
            displaySize: {x: game.canvasWidth, y: Math.max(250, game.canvasHeight * 0.35)},
            padding    : {x: 50, y: 10},
        });
    }

    menuExit(game: GameClass) {
    }
}

export const TargetMenu = new TargetMenuClass();