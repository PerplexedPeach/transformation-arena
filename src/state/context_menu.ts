import {GameClass} from "../base";
import {Action} from "../action/action";
import {ScreenPoint} from "../basetypes";
import {BaseEntity} from "../entity/player";
import {MenuItem, MenuState} from "../menu/menu";
import {SelectionItem, TextType} from "../menu/text_item";

interface NonPlayerAction {
    toString(): string;

    execute(): void;
}

export type ContextMenuItem = NonPlayerAction | Action;

export class ContextMenuClass extends MenuState {
    public actions: ContextMenuItem[];
    player: BaseEntity;

    constructor() {
        super();
    }

    _rootMenuList(): MenuItem[] {
        const menu: MenuItem[] = [];

        this.actions.forEach((item) => {
            menu.push(new SelectionItem(this.actionEnter(item).bind(this), item.toString(),
                {type: TextType.IMPORTANT}));
        });
        return menu;
    }

    actionEnter(item: ContextMenuItem) {
        return (game: GameClass, menu: MenuState) => {
            menu.restoreMenu(game);
            if (item instanceof Action) {
                // player is taking this action
                this.player.queueAction(item);
                game.paused = false;
            } else {
                // execute directly
                item.execute();
            }
        }
    }

    entry(game: GameClass, player: BaseEntity, clickScreenPt: ScreenPoint, actions: ContextMenuItem[]) {
        game.enterState(this);
        // pause game on menu selection (currently also forbids camera panning)
        game.paused = true;
        this.actions = actions;
        this.player = player;

        this.menuEntry(game, {
            location        : clickScreenPt,
            displaySize     : {x: 300, y: 300},
            padding         : {x: 10, y: 10},
            fitHeightToItems: true,
        });
    }

    menuExit(game: GameClass) {
    }
}

export const ContextMenu = new ContextMenuClass();