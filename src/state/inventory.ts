import {GameClass} from "../base";
import {BaseEntity} from "../entity/player";
import {MenuConfig, MenuItem, MenuState} from "../menu/menu";
import {HeadingItem, SelectionItem} from "../menu/text_item";
import {Clothing} from "../../lib/dad/src/clothes/clothing";
import {pushClothingList, pushClothingProperties, rarity, RARITY_TEXT_MAP} from "../menu/items";


class InventoryClass extends MenuState {
    player: BaseEntity;

    constructor() {
        super();
    }

    _rootMenuList(): MenuItem[] {
        const menu: MenuItem[] = [];
        pushClothingList.call(this, menu, this.player, this.clothesProducer, this.removeClothesEnter);

        menu.push(new HeadingItem("Inventory"));
        this.player.inv.forEach((item) => {
            let text = item.name || item.constructor.name.toLowerCase();
            const doEnter = (game: GameClass, menu: MenuState, e?: MouseEvent) => {
                if (e && e.shiftKey) {
                    const doEnter = this.wearClothesEnter(item, true);
                    doEnter.call(this, game);
                } else {
                    menu.enterMenu(this.itemProducer.bind(this, item));
                }
            };
            menu.push(new SelectionItem(doEnter, text, {type: RARITY_TEXT_MAP[rarity(item)]}));
        });
        return menu;
    }

    menuExit(game: GameClass) {
    }

    clothesProducer(clothes) {
        const newMenu: MenuItem[] = [];

        pushClothingProperties(newMenu, clothes);
        newMenu.push(new SelectionItem(this.removeClothesEnter(clothes), 'take off'));
        return newMenu;
    }

    itemProducer(item) {
        const newMenu: MenuItem[] = [];

        if (item instanceof Clothing) {
            pushClothingProperties(newMenu, item);
            newMenu.push(new SelectionItem(this.wearClothesEnter(item), 'wear'));
        }
        return newMenu;
    }

    removeClothesEnter(clothes: Clothing, justRefresh = false) {
        return (game: GameClass) => {
            if (this.player.removeClothing(clothes)) {
                this.player.inv.push(clothes);
                game.focusNeedsRedraw = true;
                this.restoreMenu(game, justRefresh);
            }
        }
    }

    wearClothesEnter(clothes: Clothing, justRefresh = false) {
        return (game: GameClass) => {
            const removedClothes = this.player.wearClothing(clothes);
            if (removedClothes) {
                removedClothes.forEach(removed => {
                    this.player.inv.push(removed);
                });
            }
            this.player.inv.splice(this.player.inv.indexOf(clothes), 1);
            game.focusNeedsRedraw = true;
            this.restoreMenu(game, justRefresh);
        }
    }


    entry(game: GameClass, player, menuConfig?: MenuConfig) {
        game.enterState(this);
        this.player = player;
        this.menuEntry(game, menuConfig);
    }

}

export const InventoryState = new InventoryClass();