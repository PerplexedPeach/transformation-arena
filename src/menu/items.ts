import {HeadingItem, SelectionItem, TextItem, TextType} from "./text_item";
import {extractUnmodifiedLocation} from "../../lib/dad/src/util/part";
import {Clothing} from "../../lib/dad/src/clothes/clothing";
import {EnterFunction, MenuItem, MenuProducer, MenuState} from "./menu";
import {getSideLocation} from "../../lib/dad/src/parts/part";
import {GameClass} from "../base";

export enum ItemRarity {
    INFERIOR = "inferior",
    COMMON = "common",
    UNCOMMON = "uncommon",
    RARE = "rare",
    LEGENDARY = "legendary"
}

export const RARITY_TEXT_MAP: Record<ItemRarity, TextType> = {
    [ItemRarity.INFERIOR] : TextType.UNIMPORTANT,
    [ItemRarity.COMMON]   : TextType.NORMAL,
    [ItemRarity.UNCOMMON] : TextType.GOOD,
    [ItemRarity.RARE]     : TextType.GOOD3,
    [ItemRarity.LEGENDARY]: TextType.GOOD4,
};

export function pushClothingProperties(newMenu: MenuItem[], clothes: Clothing) {
    const fullName = (clothes.name) ? `${clothes.name} (${clothes.constructor.name})` : `${clothes.constructor.name}`;
    newMenu.push(new HeadingItem(fullName, {type: RARITY_TEXT_MAP[rarity(clothes)]}));
    newMenu.push(new TextItem(`location: ${uniquePartLocations(clothes)}`));
    newMenu.push(new TextItem(`rarity: ${rarity(clothes)}`));
    // TODO more info about clothes
}

export function pushClothingList(menu: MenuItem[], player, menuProducer: MenuProducer,
                                 removeClothesEnter?: EnterFunction) {
    const headerItem = new HeadingItem("Wearing");
    if (removeClothesEnter) {
        headerItem
            .chain(new TextItem(" shift + click", {type: TextType.GOOD2}))
            .chain(new TextItem(" to directly wear/take off", {type: TextType.UNIMPORTANT}));
    }
    menu.push(headerItem);
    player.clothes.forEach((clothes) => {
        let text = clothes.name || clothes.constructor.name.toLowerCase();
        const sideString = getSideLocation(clothes.loc);
        if (sideString) {
            text = `${sideString} ${text}`;
        }

        const doEnter = (game: GameClass, menu: MenuState, e?: MouseEvent) => {
            if (e && e.shiftKey && removeClothesEnter) {
                const doEnter = removeClothesEnter.call(this, clothes, true);
                doEnter.call(this, game);
            } else {
                menu.enterMenu(menuProducer.bind(this, clothes));
            }
        };
        menu.push(new SelectionItem(doEnter, text, {type: RARITY_TEXT_MAP[rarity(clothes)]}));
    });
}

export function uniquePartLocations(item) {
    return Array.from(new Set(item.parts.map(part => extractUnmodifiedLocation(part.loc)))).join(", ");
}

export function rarity(item) {
    return item.rarity || ItemRarity.COMMON;
}