import {ScreenPoint} from "../basetypes";
import {AUTO_ASSIGN_SHORTCUT, DEFAULT_LINE_HEIGHT, DEFAULT_TEXT_FONT, EnterFunction, MenuItem, MenuState} from "./menu";
import {GameClass} from "../base";
import {adjustColor} from "../../lib/dad/src/util/utility";

export enum TextType {
    NORMAL,
    IMPORTANT,
    UNIMPORTANT,
    GOOD,
    GOOD2,
    GOOD3,
    GOOD4,
    BAD,
    BAD2,
    BAD3,
    LUST
}

type TextColorMap = Record<TextType, string>;
export const DEFAULT_TEXT_COLOR_MAP: TextColorMap = {
    [TextType.NORMAL]     : "#bbb",
    [TextType.IMPORTANT]  : "#fff",
    [TextType.UNIMPORTANT]: "#888",
    [TextType.GOOD]       : "#84d999",
    [TextType.GOOD2]      : "#62de59",
    [TextType.GOOD3]      : "#67ebe2",
    [TextType.GOOD4]      : "#d140d6",
    [TextType.BAD]        : "#dae37b",
    [TextType.BAD2]       : "#e0be43",
    [TextType.BAD3]       : "#e03319",
    [TextType.LUST]       : "#FF69B4",
};

export interface TextConfig {
    type: TextType;
    padding?: ScreenPoint;
    colorMap: TextColorMap;
    lineHeight: number;
    fontSize: number;
    fontStyle: string;
    desaturate: boolean;
    otherStyle?: Record<string, string>;
}

const DEFAULT_TEXT_CONFIG: TextConfig = {
    type      : TextType.NORMAL,
    colorMap  : DEFAULT_TEXT_COLOR_MAP,
    lineHeight: DEFAULT_LINE_HEIGHT,
    fontSize  : DEFAULT_LINE_HEIGHT,
    fontStyle : DEFAULT_TEXT_FONT,
    desaturate: false,
};

export class TextItem implements MenuItem {
    public readonly cfg: TextConfig;
    public selectable: boolean = false;
    private _chainedItems: TextItem[];  // to be displayed on the same line / following this content

    constructor(protected readonly text, textConfig?: Partial<TextConfig>) {
        this.cfg = {...DEFAULT_TEXT_CONFIG, ...textConfig};
        this._chainedItems = [];
    }

    get padding() {
        return this.cfg.padding;
    }

    chain(textItem: TextItem) {
        this._chainedItems.push(textItem);
        return this;
    }

    addContent(element: HTMLElement): void {
        const textElem = document.createElement('span');
        textElem.style.font = `${this.cfg.fontSize}px ${this.cfg.fontStyle}`;
        let color = this.cfg.colorMap[this.cfg.type];
        if (this.cfg.desaturate) {
            const adjusted = adjustColor(color, {s:-20, l:-20});
            if (!adjusted) {
                throw new Error("can't convert color");
            }
            color = adjusted;
        }
        textElem.style.color = color;
        if (this.cfg.otherStyle) {
            Object.entries(this.cfg.otherStyle).forEach(([style, value]) => {
                textElem.style[style] = value;
            });
        }
        textElem.innerText = this.text;
        element.appendChild(textElem);
        this._chainedItems.forEach(item => item.addContent(element));
    }

    enter(game: GameClass, menus: MenuState, e: MouseEvent) {
    }
}

export class HeadingItem extends TextItem {
    constructor(readonly text, textConfig?: Partial<TextConfig>) {
        super(text, Object.assign({type: TextType.IMPORTANT, padding: {x: 30, y: 20}}, textConfig));
    }
}


export interface SelectConfig extends TextConfig {
    shortcut: string;
    selectable: boolean;
}

const DEFAULT_SELECT_CONFIG: SelectConfig = {
    shortcut  : AUTO_ASSIGN_SHORTCUT,
    selectable: true,
    ...DEFAULT_TEXT_CONFIG
};

export class SelectionItem extends TextItem {
    public readonly shortcut;

    constructor(protected readonly doEnter: EnterFunction, text: string, userCfg?: Partial<SelectConfig>) {
        super(text, userCfg);
        const cfg = {...DEFAULT_SELECT_CONFIG, ...userCfg};
        this.selectable = cfg.selectable;
        this.shortcut = cfg.shortcut;
    }

    enter(game, menus: MenuState, e: MouseEvent) {
        this.doEnter(game, menus, e);
    }
}
