import {ScreenPoint} from "../basetypes";
import {State} from "../state/state";
import {GameClass} from "../base";
import {Keys} from "../input/keyboard";
import {getClickScreenPosition, Mouse} from "../input/mouse";
import {pointInRectangle} from "../util/math";

export const AUTO_ASSIGN_SHORTCUT = '@'; // unused shortcut key to indicate that this shortcut should be assigned
export const DEFAULT_LINE_HEIGHT = 18;
export const DEFAULT_TEXT_FONT = "monospace";

export interface MenuItem {
    shortcut?: string;
    padding?: ScreenPoint;
    selectable: boolean;

    addContent(element: HTMLElement): void;

    enter(game: GameClass, menus: MenuState, e?: MouseEvent): void;
}

export type EnterFunction = MenuItem["enter"];
type MenuList = MenuItem[];

export interface MenuConfig {
    location?: ScreenPoint;
    displaySize?: ScreenPoint;
    padding?: ScreenPoint;
    backgroundColor?: string;
    fitHeightToItems?: boolean;
}

export const DEFAULT_MENU_CONFIG: MenuConfig = {
    location        : {x: 0, y: 0},
    padding         : {x: 30, y: 5},
    backgroundColor : "hsla(0, 0%, 0%, 0.8)",
    fitHeightToItems: false,
};

export type MenuProducer = () => MenuList;

export abstract class MenuState extends State {
    private _menuHistory: MenuProducer[];
    private _keyRegister: Record<string, MenuItem>;
    private _baseElem: HTMLElement;

    game: GameClass;
    cfg: Required<MenuConfig>;
    size: ScreenPoint;

    currentPage: number;

    protected constructor() {
        super();
        const base = document.getElementById("menu-dom");
        if (base) {
            this._baseElem = base;
        } else {
            throw new Error("Can't find menu-dom element");
        }
    }

    abstract _rootMenuList(): MenuList;

    abstract menuExit(game: GameClass);

    menuEntry(game: GameClass, menuConfig?: MenuConfig) {
        game.paused = true;
        this.game = game;

        // @ts-ignore
        this.cfg = Object.assign({}, DEFAULT_MENU_CONFIG, menuConfig);
        this.cfg.displaySize = this.cfg.displaySize || {x: game.canvasWidth, y: game.canvasHeight};
        this.currentPage = 0;


        this._menuHistory = [];
        this.enterMenu(this._rootMenuList.bind(this));
    }

    placeMenu(menuList: MenuList) {
        this._baseElem.innerHTML = '';
        this._baseElem.style.backgroundColor = this.cfg.backgroundColor;

        this._keyRegister = {};

        for (let item of menuList) {
            if (item.shortcut && item.shortcut !== AUTO_ASSIGN_SHORTCUT) {
                if (item.shortcut in this._keyRegister) {
                    throw new Error(`duplicate shortcut ${item.shortcut}`);
                }
                this._keyRegister[item.shortcut] = item;
            }
        }

        // auto assign shortcuts
        let shortcut = 'a';
        for (let item of menuList) {
            // allow items that are not selectable to have generated shortcuts
            if (item.shortcut && item.shortcut === AUTO_ASSIGN_SHORTCUT) {
                while (shortcut in this._keyRegister) {
                    shortcut = String.fromCharCode(shortcut.charCodeAt(0) + 1);
                }
                item.shortcut = shortcut;
                this._keyRegister[item.shortcut] = item;
                shortcut = String.fromCharCode(shortcut.charCodeAt(0) + 1);
            }
        }

        const shortcutFont = `${DEFAULT_LINE_HEIGHT}px ${DEFAULT_TEXT_FONT}`;
        const textIndent = DEFAULT_LINE_HEIGHT * 2.5;
        // create DOM elements
        for (let item of menuList) {
            const div = document.createElement('div');
            div.style.textIndent = `${-textIndent}px`;
            if (item.selectable) {
                div.classList.add('selectable');
                div.onclick = (e) => {
                    item.enter(this.game, this, e)
                };
            }
            const padding = item.padding || this.cfg.padding;
            div.style.padding = `${padding.y}px ${padding.x}px ${padding.y}px ${padding.x + textIndent}px`;

            const shortcutElem = document.createElement('span');
            shortcutElem.classList.add('shortcut');
            if (item.selectable && item.shortcut) {
                shortcutElem.innerText = `${item.shortcut} - `;
            } else {
                shortcutElem.innerText = `    `;
            }
            shortcutElem.style.font = shortcutFont;
            div.appendChild(shortcutElem);

            item.addContent(div);
            this._baseElem.appendChild(div);
        }

        if (!this.cfg.fitHeightToItems) {
            this._baseElem.style.height = `${this.cfg.displaySize.y}px`;
        } else {
            this._baseElem.style.height = "inherit";
        }

        // keep the menu fully displayed on the screen
        this.cfg.location.x -= Math.max(0, this.cfg.location.x + this.cfg.displaySize.x - this.game.canvasWidth);
        this.cfg.location.y -= Math.max(0, this.cfg.location.y + this._baseElem.offsetHeight - this.game.canvasHeight);
        this._baseElem.style.marginLeft = `${this.cfg.location.x}px`;
        this._baseElem.style.marginTop = `${this.cfg.location.y}px`;
        this._baseElem.style.width = `${this.cfg.displaySize.x}px`;
        this._baseElem.style.display = 'block';
    }

    render(game: GameClass) {
        // use DOM's rendering
    }

    handleKeyDown(game: GameClass, keys: Keys) {
        // only allow one menu to be accessed
        if (keys["Escape"]) {
            this.restoreMenu(game);
            return;
        }

        for (let [key, pressed] of Object.entries(keys)) {
            if (!pressed) {
                continue;
            }
            const item = this._keyRegister[key];
            if (item && item.selectable) {
                item.enter(game, this);
                break;
            }
        }
    }

    handleMouseUp(game, e) {
        if (e.button === Mouse.LEFT) {
            // get world position of click
            const screenPos = getClickScreenPosition(game, e);
            // check if clicked inside menu at all
            if (!pointInRectangle({topLeft: this.cfg.location, size: this.cfg.displaySize}, screenPos)) {
                this.clearMenu(game);
            }
        }
    }

    handleContextMenu(game: GameClass, screenPoint: ScreenPoint) {
        if (!pointInRectangle({topLeft: this.cfg.location, size: this.cfg.displaySize}, screenPoint)) {
            this.clearMenu(game);
            game.state.handleContextMenu(game, screenPoint);
        }
    }

    restoreMenu(game: GameClass, justRefresh=false) {
        if (!justRefresh) {
            this._menuHistory.pop();
        }
        // on the last menu and restoring means we'll just exit
        if (this._menuHistory.length === 0) {
            this.clearMenu(game);
        } else {
            this._updateMenuList();
        }
    }

    enterMenu(menuProducer: MenuProducer) {
        this._menuHistory.push(menuProducer);
        this._updateMenuList();
    }

    clearMenu(game: GameClass) {
        this.menuExit(game);
        game.restoreState();
        this._baseElem.style.display = 'none';
    }

    _updateMenuList() {
        this.placeMenu(this._menuHistory[this._menuHistory.length - 1].call(this));
    }
}