import {FixedDurationAction} from "./action";
import {getUnitVector, scale} from "drawpoint/dist-esm";
import {StrikeCapable} from "../mixins/strikeable";
import {Point} from "../basetypes";
import {DamageablePart} from "../mixins/damageable";
import {Entity, UID} from "../entity/entity";
import {hasMixin} from "ts-mixer";
import {randomChoice} from "../util/util";
import {logMsg} from "../log";
import {TextType} from "../menu/text_item";

/**
 * Strike with a strike capable object in a direction at a fixed speed
 */
export class Strike extends FixedDurationAction {
    protected _hit: Set<UID>;

    constructor(actor, public striker: StrikeCapable, public dir: Point,
                readonly _target?: Entity, readonly _targetPart?: DamageablePart, readonly _cleave = false) {
        super(actor, striker.strikeDuration(actor));
        this.dir = getUnitVector(dir);
        if (this._targetPart && !this._target) {
            throw new Error("target entity must be specified if target part is specified");
        }
        this._hit = new Set<UID>();
    };

    actFinish(): void {
    }

    actProgress(progress: number): void {
        if (progress > 0.5) {
            progress = 1 - progress;
        }
        this.striker.displacement = scale(this.dir, this.striker.strikeRange(this.actor) * 2 * progress);
        // if we hit something and have to stop the forward action
        this.checkForHit();
    }

    checkForHit() {
        const strikeShape = this.striker.strikeShape;
        // already hit someone and not cleaving
        if (!this._cleave && this._hit.size) {
            return;
        }
        if (this._target) {
            // don't hit multiple times if we've hit them already
            if (this._hit.has(this._target.uid)) {
                return;
            }
            // know who we're targeting, only check against hitting this entity
            if (strikeShape.hitsEntity(this.actor, this.striker, this._target, this._targetPart)) {
                this._hit.add(this._target.uid);
                // regardless if we did any damage or not register the hit so we can't try hitting them again
                this.applyDamage(this.actor, this.striker, this._target, this._targetPart);
            }
        } else {
            // otherwise check for entities close to this position
            for (let entity of this.actor.map.entities()) {
                if (entity === this.actor) {
                    continue;
                }
                if (this._hit.has(entity.uid)) {
                    continue;
                }
                if (strikeShape.hitsEntity(this.actor, this.striker, entity)) {
                    this._hit.add(entity.uid);
                    this.applyDamage(this.actor, this.striker, entity);
                    // only hit first target if we're not cleaving
                    if (!this._cleave) {
                        break;
                    }
                }
            }
        }
    }

    applyDamage(strikerParent, striker: StrikeCapable, target: Entity, targetPart?: DamageablePart) {
        if (!targetPart) {
            // randomly select a part if not given any
            const candidateParts: DamageablePart[] = [];
            for (let part of target.parts) {
                if (hasMixin(part, DamageablePart)) {
                    candidateParts.push(part);
                }
            }
            targetPart = randomChoice(candidateParts);
        }

        // no available part for hit
        if (!targetPart) {
            return;
        }

        const hitProperties = striker.calculateHit(strikerParent, target, targetPart);
        if (Math.random() < hitProperties.probability) {
            const damage = targetPart.takeDamage(hitProperties.damage);
            // TODO flavor text for how much damage the attack did
            logMsg({
                sender  : strikerParent,
                receiver: target,
                type    : TextType.GOOD3,
                text    : `${strikerParent.name} ${striker.strikeTerm}s ${target.name}'s ${targetPart.toString()} for ${damage} damage`
            });
        } else {
            logMsg({
                sender  : strikerParent,
                receiver: target,
                type    : TextType.BAD2,
                text    : `${strikerParent.name} tries to ${striker.strikeTerm} ${target.name}'s ${targetPart.toString()} but misses`
            });
        }
    }
}


