import {Action, ActionState} from "./action";
import {distPoint} from "../util/math";
import {Point} from "../basetypes";
import {diff, getUnitVector} from "drawpoint/dist-esm";
import {Actor} from "../mixins/actor";
import {hasMixin} from "ts-mixer";
import {Moveable} from "../mixins/moveable";

const CLOSE_ENOUGH = 5; // pixel resolution

/**
 * Move in a straight line to target x,y (world)
 */
export class Move extends Action {
    constructor(actor, public destination: Point) {
        super(actor);
    };

    toString(): string {
        return "Move to here";
    }

    act(delta: number): boolean {
        if (!hasMixin(this.actor, Moveable)) {
            this.state = ActionState.FINISHED;
            return false;
        }

        // move has equal
        if (this.state === ActionState.UNSTARTED) {
            this.state = ActionState.STARTED;
        }


        const dxy = getUnitVector(diff(this.actor, this.destination));

        // TODO path finding around obstacles?

        const moved = this.actor.move(delta, dxy);

        const togo = distPoint(this.destination, this.actor);
        if (togo < CLOSE_ENOUGH) {
            this.state = ActionState.FINISHED;
        }
        return moved;
    }
}

export function changeMove(actor: Actor, pt: Point) {
    const currentAction = actor.getUpcomingActionOfType(Move);
    if (currentAction) {
        currentAction.destination = pt;
    } else {
        actor.queueAction(new Move(actor, pt));
    }
}
