import {ActionState, FixedDurationAction} from "./action";
import {add, angle, diff, getUnitVector, norm, scale} from "drawpoint/dist-esm";
import {Point} from "../basetypes";
import {Entity} from "../entity/entity";
import {hasMixin} from "ts-mixer";
import {GraspCapable} from "../mixins/graspable";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {BaseEntity} from "../entity/player";
import {Arousable} from "../mixins/arousable";
import {logMsg} from "../log";
import {TextType} from "../menu/text_item";
import {LickCapable} from "../mixins/lickable";
import {InsertCapable, Orifice} from "../mixins/insertable";

const SEX_ATTACK_DURATION = 0.3;

/**
 * Strike with a strike capable object in a direction at a fixed speed
 */
export class Rub extends FixedDurationAction {
    constructor(actor, public rubber: GraspCapable, public dir: Point, readonly target: Entity,
                readonly targetPart: Arousable & BodyPart) {
        super(actor, SEX_ATTACK_DURATION);
        // TODO rub in a more systematic way? currently in random direction
        this.dir = getUnitVector(add(dir, {x: Math.random() - 0.5, y: Math.random() - 0.5}, 4));
    };

    actFinish(): void {
        const [arousal, pleasure] = this.targetPart.arouse(this.target, this.rubber.rubArousal(this.actor));
        logMsg({
            sender  : this.actor,
            receiver: this.target,
            type    : TextType.LUST,
            text    : `${this.actor.name} ${this.rubber.rubTerm(
                this.targetPart)}s ${this.target.name}'s ${this.targetPart.toString()} for ${arousal} arousal and ${pleasure} pleasure`
        });
    }

    actProgress(progress: number): void {
        if (progress > 0.5) {
            progress = 1 - progress;
        }
        this.rubber.displacement = scale(this.dir, this.rubber.graspRange(this.actor) * progress);
    }
}

export class Lick extends FixedDurationAction {
    constructor(actor, public licker: LickCapable, readonly target: Entity, readonly targetPart: Arousable & BodyPart) {
        super(actor, SEX_ATTACK_DURATION);
    };

    actFinish(): void {
        const [arousal, pleasure] = this.targetPart.arouse(this.target, this.licker.lickArousal(this.actor));
        logMsg({
            sender  : this.actor,
            receiver: this.target,
            type    : TextType.LUST,
            text    : `${this.actor.name} licks ${this.target.name}'s ${this.targetPart.toString()} for ${arousal} arousal and ${pleasure} pleasure`
        });
    }

    actProgress(progress: number): void {
        moveTowards(this.actor, this.target, this.licker.lickRange(), progress);
    }
}


export class Insert extends FixedDurationAction {
    constructor(actor, public inserter: InsertCapable, readonly target: Entity,
                readonly targetPart: Arousable & Orifice & BodyPart) {
        super(actor, SEX_ATTACK_DURATION);
    };

    actFinish(): void {
        // TODO compute hit chance based on presence of ongoing grapple
        const [arousal, pleasure] = this.targetPart.arouse(this.target, this.inserter.insertArousal(this.actor));
        logMsg({
            sender  : this.actor,
            receiver: this.target,
            type    : TextType.LUST,
            text    : `${this.actor.name} fucks ${this.target.name}'s ${this.targetPart.toString()} for ${arousal} arousal and ${pleasure}`
        });
    }

    actProgress(progress: number): void {
        moveTowards(this.actor, this.target, this.inserter.insertRange(), progress);
    }
}


function moveTowards(this: void, actor: Entity, target: Entity, minDist: number, progress: number) {
    const dxy = diff(target, actor);
    // face the target
    if (hasMixin(actor, BaseEntity)) {
        actor.rot = Math.PI + angle(dxy);
    }
    const distTo = norm(dxy);
    if (distTo > minDist) {
        const closestPointAtDist = add(target, dxy, minDist / distTo * progress);
        actor.x = closestPointAtDist.x;
        actor.y = closestPointAtDist.y;
    }
}

export class Orgasm extends FixedDurationAction {
    constructor(actor, public readonly orgasmPart: Arousable, public _duration: number) {
        super(actor, _duration);
        // TODO reduce actor's effective attributes why they are having an orgasm so they can't dodge
        logMsg({
            sender  : this.actor,
            receiver: this.actor,
            type    : TextType.LUST,
            text    : `${this.actor.name} cums from their ${this.orgasmPart.toString()} and is helpless while basking in the afterglow!`
        });
    };

    actFinish(): void {
    }

    actProgress(progress: number): void {
    }
}

