import {FixedDurationAction} from "./action";
import {add, getUnitVector, scale} from "drawpoint/dist-esm";
import {Point} from "../basetypes";
import {Entity} from "../entity/entity";
import {hasMixin} from "ts-mixer";
import {Graspable, GraspCapable, OngoingGrasp} from "../mixins/graspable";
import {BodyPart} from "../../lib/dad/src/parts/part";
import {BaseEntity} from "../entity/player";
import {EntitySkills, scaleOnSkill} from "../entity/skills";
import {Attribute, scaleOnAtt} from "../entity/attributes";
import {DamageablePart} from "../mixins/damageable";
import {logMsg} from "../log";
import {TextType} from "../menu/text_item";

const START_GRASP_DELAY = 0.3;
const STRUGGLE_GRASP_DURATION = 0.1;
const RNG_HELP_STRENGTH_MAX = 3;

/**
 * Strike with a strike capable object in a direction at a fixed speed
 */
export class Wrestle extends FixedDurationAction {
    constructor(actor, public grasper: GraspCapable, public dir: Point,
                readonly target: Entity, readonly targetPart: Graspable & BodyPart) {
        super(actor, START_GRASP_DELAY);
        if (!(dir.x === 0 && dir.y === 0)) {
            this.dir = getUnitVector(dir);
        }
    };

    actFinish(): void {
        // update ongoing grasp objects on both target and actor
        const thisGrasp: OngoingGrasp = {
            grasper: this.grasper,
            grasped: this.targetPart,
            actor  : this.actor,
            target : this.target
        };
        const ongoing = this.actor.wrestling.find(
            grasp => grasp.grasper === thisGrasp.grasper && grasp.grasped === thisGrasp.grasped);

        // apply damage if ongoing; otherwise start grasp
        if (ongoing) {
            if (hasMixin(this.targetPart, DamageablePart)) {
                const damage = this.targetPart.takeDamage(this.grasper.pinchDamage(this.actor));
                logMsg({
                    sender  : this.actor,
                    receiver: this.target,
                    type    : TextType.GOOD3,
                    text    : `${this.actor.name} pinches ${this.target.name}'s ${this.targetPart.toString()} for ${damage} damage`
                });
            }
        } else {
            // TODO implement wrestle defense/avoidance
            logMsg({
                sender  : this.actor,
                receiver: this.target,
                type    : TextType.GOOD3,
                text    : `${this.actor.name} grasps ${this.target.name}'s ${this.targetPart.toString()}`
            });
            this.actor.wrestling.push(thisGrasp);
            if (hasMixin(this.target, BaseEntity)) {
                this.target.wrestling.push(thisGrasp);
            }
        }
    }

    actProgress(progress: number): void {
        this.grasper.displacement = scale(this.dir, this.grasper.graspRange(this.actor) * progress);
    }
}

export class StopWrestle extends FixedDurationAction {
    protected _escapedAll: boolean;
    protected _theirPartsToRetrieve: GraspCapable[];

    constructor(actor, readonly target: Entity) {
        super(actor, STRUGGLE_GRASP_DURATION);

        // success depends on escaping all grasps from the target
        const escapedGrasps: OngoingGrasp[] = [];
        for (let grasp of this.actor.wrestling) {
            if (grasp.actor === target && grasp.target === this.actor) {
                // test for grasp escape (str based) - RNG can also apply
                const graspStrength = grasp.grasper.graspStrength(grasp.actor);
                const graspEscapeStrength = scaleOnAtt(grasp.target, Attribute.STR, 8, 20) * scaleOnSkill(grasp.target,
                    EntitySkills.WRESTLING, 1, 2);

                if (graspEscapeStrength + Math.random() * RNG_HELP_STRENGTH_MAX > graspStrength) {
                    escapedGrasps.push(grasp);
                }
            }
        }

        // should be ok to compare object references here (as long as we don't serialize the references)
        this.actor.wrestling = this.actor.wrestling.filter(grasp => !escapedGrasps.includes(grasp));
        this.target.wrestling = this.target.wrestling.filter(grasp => !escapedGrasps.includes(grasp));
        // parts that we know should return to 0 displacement
        this._theirPartsToRetrieve = escapedGrasps.map(grasp => grasp.grasper);

        // we escape grasps initiated by ourselves for free
        // we only break our own grasps if we successfully escape all their grasps
        this._escapedAll = !this.actor.wrestling.find(grasp => grasp.actor === target && grasp.target);
    };

    actFinish(): void {
        // finally escape our own grasps if we escaped all theirs
        if (this._escapedAll) {
            const ourStoppedGrasps = this.actor.wrestling.filter(
                grasp => grasp.actor === this.actor && grasp.target === this.target);
            this.actor.wrestling = this.actor.wrestling.filter(grasp => !ourStoppedGrasps.includes(grasp));
            this.target.wrestling = this.target.wrestling.filter(grasp => !ourStoppedGrasps.includes(grasp));
            ourStoppedGrasps.forEach(grasp => {
                grasp.grasper.displacement = {x: 0, y: 0};
            });
        }
    }

    actProgress(progress: number): void {
        this._theirPartsToRetrieve.forEach(part => {
            // geometrically retrieve part
            part.displacement = add({x: 0, y: 0}, part.displacement, 1 - progress);
        });
    }
}

