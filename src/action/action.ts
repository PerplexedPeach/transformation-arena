import {Actor} from "../mixins/actor";
import {Entity} from "../entity/entity";

export enum ActionState {
    UNSTARTED = 1,
    STARTED,
    FINISHED,
}

/**
 * the API to actions is that they all must have an act method taking delta
 * it's cumbersome in JS to use the interface pattern, so it's left as a given
 * they will also have a isFinished method to be true when finished
 */
export abstract class Action {
    public state: ActionState;

    protected constructor(protected actor: Actor & Entity) {
        this.state = ActionState.UNSTARTED;
    }

    /**
     * Perform action
     * @param delta time elapsed
     * @return whether the action had any effect
     */
    abstract act(delta: number): boolean;
}

export abstract class FixedDurationAction extends Action {
    protected _elapsed: number;
    protected constructor(actor, protected readonly _duration: number) {
        super(actor);
        this._elapsed = 0;
    }

    act(delta: number): boolean {
        if (this.state === ActionState.UNSTARTED) {
            this.state = ActionState.STARTED;
        }

        this._elapsed += delta;
        let progress = this._elapsed / this._duration;
        if (progress >= 1) {
            this.state = ActionState.FINISHED;

            this.actFinish();
        }

        this.actProgress(progress);
        return true;
    }

    abstract actFinish(): void;
    abstract actProgress(progress: number): void;
}