import {Game} from "./base";
import {load} from "../lib/dad/src";

export * from "./base";

window.onload = function() {
    load().then(() => {
        Game.run();
    });
};
